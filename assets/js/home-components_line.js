$(function() {
    function e() {
        $(".swiper-slide, .swiper-slide a, .swiper-slide button").attr("tabindex", "-1"), $(".swiper-slide-active a, .swiper-slide-active button").attr("tabindex", "0")
    }

    function t(e, t, i, a) {
        if (">" == a) {
            if ($(window).width() > i) return void(e.hasClass("slick-initialized") && e.slick("unslick"));
            if (!e.hasClass("slick-initialized")) return e.slick(t)
        } else {
            if ($(window).width() < i) return void(e.hasClass("slick-initialized") && e.slick("unslick"));
            if (!e.hasClass("slick-initialized")) return e.slick(t)
        }
        if (!e.hasClass("slick-initialized")) return e.slick(t)
    }

    function i() {
        $(".rooms-info .inset-rotation").each(function() {
            var e = $(this);
            e.find("img").length > 1 && e.slick({
                dots: !0,
                arrows: !1,
                slide: "img",
                draggable: !1,
                swipe: !1
            })
        })
    }

    function a() {
        $(".swiper-container").each(function() {
            $(this)[0].swiper && $(this)[0].swiper.destroy()
        }), i();
        new Swiper(".swiper-container", {
            preloadImages: !1,
            lazyLoading: !0,
            lazyLoadingInPrevNext: !0,
            nextButton: ".swiper-button-next",
            prevButton: ".swiper-button-prev",
            loop: !0,
            slidesPerView: "auto",
            centeredSlides: !0,
            speed: 1300,
            spaceBetween: 0,
            effect: "coverflow",
            autoHeight: !1,
            a11y: !0,
            coverflow: {
                rotate: 0,
                stretch: 0,
                depth: 0,
                modifier: 1,
                slideShadows: !1
            },
            breakpoints: {
                1024: {
                    spaceBetween: 24
                }
            },
            onInit: function(t) {
                e()
            },
            onDestroy: function() {
                $(".rooms-info .inset-rotation").unslick()
            },
            onSlideChangeEnd: function(t) {
                e()
            }
        })
    }

    function o() {
        var e = new TimelineLite({
                reversed: !0
            }),
            t = $("#masthead").height(),
            i = $("#masthead h1").height(),
            a = $("#masthead h1").position().top,
            o = t - (a + i);
        o -= 72, o += "px", e.from(CSSRulePlugin.getRule("#masthead:after"), .5, {
            height: "111px",
            ease: Power0.easeNone
        }), e.from(CSSRulePlugin.getRule("#masthead:before"), 2, {
            height: o,
            opacity: 1,
            cssRule: {
                top: "calc(100% - " + o + ")"
            }
        })
    }

    function n() {
        var e = new TimelineLite;
        e.to(CSSRulePlugin.getRule(".content-area-main > article > .location-overview header:before"), .75, {
            width: "95px",
            ease: Power0.easeNone,
            delay: 1.25
        }), e.to(CSSRulePlugin.getRule(".content-area-main > article > .location-overview header:after"), .5, {
            width: "60px",
            ease: Power0.easeNone
        }), TweenLite.to(CSSRulePlugin.getRule(".content-area-main > article > .location-overview table tbody"), 2.75, {
            cssRule: {
                borderColor: "#D8D8D8"
            }
        }), TweenLite.to(CSSRulePlugin.getRule(".content-area-main > article > .location-overview table tbody tr:not(:last-child)"), 2.75, {
            cssRule: {
                borderColor: "#D8D8D8"
            }
        }), TweenLite.to(CSSRulePlugin.getRule(".content-area-main > article > .location-overview table tbody tr td"), 2.75, {
            cssRule: {
                borderColor: "#D8D8D8"
            }
        }), TweenLite.to(CSSRulePlugin.getRule(".content-area-main > article > .location-overview table tbody tr td:first-child"), 2.75, {
            cssRule: {
                borderColor: "#D8D8D8"
            }
        }), TweenLite.to(CSSRulePlugin.getRule(".content-area-main > article > .location-overview table tbody tr td.first"), 2.75, {
            cssRule: {
                borderColor: "#D8D8D8"
            }
        })
    }

    function r() {
        var e = new TimelineLite({
            reversed: !0
        });
        e.from(CSSRulePlugin.getRule(".fan-social .large-order-2 header:after"), .5, {
            width: "46px",
            ease: Power0.easeNone
        }), e.from(CSSRulePlugin.getRule(".fan-social .large-order-2 header:before"), .75, {
            width: "69px",
            opacity: 1,
            ease: Power0.easeNone
        })
    }

    function l() {
        $(window).width() >= 1024 ? (TweenLite.to(CSSRulePlugin.getRule(".fan-social .social-grid .bordered-transparent:before"), .75, {
            height: "50px",
            opacity: 1,
            ease: Power0.easeNone,
            delay: 1.15
        }), TweenLite.to(CSSRulePlugin.getRule(".fan-social .social-grid .bordered-transparent:after"), 1, {
            height: "50px",
            opacity: 1,
            ease: Power0.easeNone,
            delay: 1.9
        })) : (TweenLite.to(CSSRulePlugin.getRule(".fan-social .social-grid .bordered-transparent:before"), .5, {
            height: "20px",
            opacity: 1,
            ease: Power0.easeNone,
            delay: 1.15
        }), TweenLite.to(CSSRulePlugin.getRule(".fan-social .social-grid .bordered-transparent:after"), .75, {
            height: "35px",
            opacity: 1,
            ease: Power0.easeNone,
            delay: 1.9
        }))
    }

    function s() {
        var e = new TimelineLite;
        e.to(CSSRulePlugin.getRule(".content-area-main > article > .explore .row .info .counter div:before"), .7, {
            width: "72px",
            ease: Power0.easeNone,
            delay: 1.5
        }), e.to(CSSRulePlugin.getRule(".content-area-main > article > .explore .row .info .counter div:after"), .35, {
            width: "19px",
            ease: Power0.easeNone
        }), $(window).width() >= 1024 && TweenLite.to(CSSRulePlugin.getRule(".content-area-main > article > .explore .row .info .counter:before"), 1.25, {
            opacity: 1,
            delay: 2.75
        }), TweenLite.to(CSSRulePlugin.getRule(".content-area-main > article > .explore > header:before"), 1.25, {
            width: "165px",
            opacity: 1,
            ease: Power0.easeNone,
            delay: .5
        }), TweenLite.to(CSSRulePlugin.getRule(".content-area-main > article > .explore .row .info"), 2.75, {
            cssRule: {
                borderColor: "#c1b8b8"
            }
        }), TweenLite.to(CSSRulePlugin.getRule(".content-area-main > article > .explore .row .info .columns"), 2.75, {
            cssRule: {
                borderColor: "#c1b8b8"
            }
        })
    }

    function c() {
        TweenLite.to(CSSRulePlugin.getRule(".content-area-main > article > .push-console > header:before"), 3.25, {
            width: "67.5%",
            opacity: 1,
            ease: Power0.easeNone,
            delay: .5
        })
    }

    function d() {
        $(window).width() >= 1024 ? ($(".expanding-push .push-item .inner").on({
            mouseenter: function() {
                $(this).parents(".push-item").addClass("hovered")
            },
            mouseleave: function() {
                $(this).parents(".push-item").removeClass("hovered")
            }
        }), $(".expanding-push .push-item").each(function() {
            $(this).removeClass("active large-5 large-expand"), $(this).hasClass("hide-for-medium-only") ? $(this).addClass("active large-5") : $(this).addClass("large-expand")
        })) : ($(".expanding-push .push-item .inner").on({
            mouseenter: function() {
                $(this).parents(".push-item").removeClass("hovered")
            },
            mouseleave: function() {
                $(this).parents(".push-item").removeClass("hovered")
            }
        }), $(".expanding-push .push-item").each(function() {
            $(this).removeClass("active large-5"), $(this).hasClass("medium-8") && ($(this).addClass("active large-5"), $(this).removeClass("large-expand"))
        })), $(window).width() < 640 && $(".expanding-push .push-item").each(function() {
            $(this).removeClass("active")
        }), $(".push-item .rollover h2 a").on("click", function(e) {
            if ($(this).parents(".push-item").hasClass("active")) return !0;
            e.preventDefault()
        })
    }
    $(document).on("click", ".focus-link", function(e) {
        e.preventDefault();
        var t = $("#masthead-push .slick-active");
        "link" == t.attr("data-push-type") ? t.attr("target") ? window.open(t.attr("href")) : window.open(t.attr("href"), "_self") : t.trigger("click")
    }), $("#masthead-push").slick();
    var f = $("#masthead-push .slick-active").text();
    $(".focus-link em").text(f), $("#masthead-push").on("afterChange", function(e, t, i, a) {
        var o = $("#masthead-push .slick-active").text();
        $(".focus-link em").text(o)
    }), $(document).on("click", "#masthead-push a", function(e) {
        var t = $(this),
            i = $("#masthead-popup"),
            a = $("#masthead-popup .video"),
            o = $("#masthead-popup .lightbox"),
            n = $(this).text();
        if ("lightbox" == t.attr("data-push-type")) {
            var r = t.attr("data-push-lightbox-header-tag"),
                l = t.attr("data-push-lightbox-header-title"),
                s = t.attr("data-push-lightbox-content");
            o.find("h1").html(r), o.find("h2").html(l), o.find("p").html(s), a.hide(), a.empty(), o.show(), i.removeClass("video-on")
        } else if ("video" == t.attr("data-push-type")) {
            var c = t.attr("data-video-url");
            c.indexOf(".mp4") >= 0 ? a.html('<iframe src="' + c + '" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>') : a.html('<img src="' + c + '" alt="' + n + '" />'), a.show(), o.hide(), i.addClass("video-on")
        }
    }), $("#masthead-popup").on("closed.zf.reveal", function() {
        $("#masthead-popup .video").empty()
    }), $(".location-overview .inset").slick({
        rtl: "rtl" == $("html").attr("dir"),
        lazyLoad: "progressive",
        slidesToShow: 1,
        slidesToScroll: 1
    }), $.fn.unslick = function() {
        return this.each(function(e, t) {
            t.slick && t.slick.destroy()
        })
    };
    $("#example-tabs").length && new Foundation.Tabs($("#example-tabs")), $("#example-tabs").on("change.zf.tabs", function() {
        a()
    });
    var w = !1;
    if ($(window).bind("resize", function() {
            !1 !== w && clearTimeout(w), w = setTimeout(function() {
                t($("#instafeed"), {
                    infinite: !0,
                    slidesToShow: 3,
                    slidesToScroll: 3
                }, 768, ">"), a(), d(), $(window).width() >= 1024 ? (TweenLite.to(CSSRulePlugin.getRule(".content-area-main > article > .explore .row .info .counter:before"), 1.25, {
                    opacity: 1,
                    delay: 0
                }), TweenLite.to(CSSRulePlugin.getRule(".fan-social .social-grid .bordered-transparent:before"), 0, {
                    height: "50px",
                    opacity: 1,
                    ease: Power0.easeNone
                }), TweenLite.to(CSSRulePlugin.getRule(".fan-social .social-grid .bordered-transparent:after"), 0, {
                    height: "50px",
                    opacity: 1,
                    ease: Power0.easeNone
                })) : (TweenLite.to(CSSRulePlugin.getRule(".fan-social .social-grid .bordered-transparent:before"), 0, {
                    height: "20px",
                    opacity: 1,
                    ease: Power0.easeNone
                }), TweenLite.to(CSSRulePlugin.getRule(".fan-social .social-grid .bordered-transparent:after"), 0, {
                    height: "35px",
                    opacity: 1,
                    ease: Power0.easeNone
                }))
            }, 300)
        }), a(), $(".explore .align-middle").filter(":even").each(function() {
            $(this).children(".inset").addClass("large-order-2"), $(this).children(".info").addClass("large-order-1")
        }), window.sr = ScrollReveal({
            mobile: !1
        }), function() {
            return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
        }() && (o(), n(), r(), l(), s(), c()), $(window).on("resize", function() {
            var e = $("#masthead").height(),
                t = $("#masthead h1").height(),
                i = $("#masthead h1").position().top,
                a = e - (i + t);
            a += "px", new TimelineLite({
                reversed: !0
            }).from(CSSRulePlugin.getRule("#masthead:before"), 0, {
                height: a,
                cssRule: {
                    top: "calc(100% - " + a + ")"
                }
            })
        }), sr.reveal("#masthead", {
            distance: "10px",
            scale: .75,
            duration: 650,
            viewFactor: .0125,
            // delay: 400,
            beforeReveal: function() {
                o()
            }
        }), sr.reveal(".location-overview", {
            distance: "10px",
            scale: .75,
            duration: 650,
            viewFactor: .0125,
            beforeReveal: function() {
                n()
            }
        }), sr.reveal(".rooms-info", {
            distance: "10px",
            scale: .75,
            duration: 650,
            viewFactor: .0125,
            beforeReveal: function() {}
        }), sr.reveal(".fan-social", {
            distance: "10px",
            scale: .75,
            duration: 650,
            viewFactor: .0125,
            beforeReveal: function() {
                r()
            }
        }), sr.reveal(".twitter-block", {
            distance: "10px",
            scale: .75,
            duration: 0,
            opacity: 1,
            scale: 1,
            viewFactor: 0,
            distance: 0,
            beforeReveal: function() {
                l()
            }
        }), sr.reveal(".explore", {
            distance: "10px",
            scale: .75,
            duration: 650,
            viewFactor: .0125,
            beforeReveal: function() {
                s()
            }
        }), sr.reveal(".push-console", {
            distance: "10px",
            scale: .75,
            duration: 650,
            viewFactor: .0125,
            beforeReveal: function() {
                c()
            }
        }), TweenLite.to(CSSRulePlugin.getRule(".booking-console .console .fieldset"), 4, {
            cssRule: {
                borderColor: "#675a5a"
            }
        }), d()) {}
    $(window).on("load", function() {
        var e, t = window.location.hash;
        if (t) {
            var i = $('.tabs a[data-category="' + t.substring(1).replace(/-/g, " ") + '"]');
            i.length && (window.innerWidth >= 1024 ? (i.trigger("click"), e = i.parents(".tabs").offset().top - 500) : (i.parents(".tabs").siblings("select").val(t.substring(1)).trigger("change"), e = i.parents(".tabs").parent().offset().top - 80))
        }
        e && $(window).scrollTop(e)
    })
});
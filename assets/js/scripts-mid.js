function navback() {
    $(".nav-back").off("click.navback"), $(".nav-back").on("click.navback", function(e) {
        e.preventDefault();
        var t = $(this),
            i = t.parent().closest(".treelevel"),
            a = i.parent(),
            n = a.parent();
        if (a.removeClass("moved"), n.not(".cities").css("left", "0"), i.hasClass("level-0") && $(".mobile-nav").removeClass("secondary-visible"), i.hasClass("level-1")) {
            var o = $(".mobile-nav");
            o.removeClass("tertiary-visible").addClass("secondary-visible"), $(".region").removeClass("active inactive"), $(".region").find(".city").each(function() {
                var e = $(this);
                e.removeClass("has-dropdown");
                var t = e.find("> a"),
                    i = t.attr("data-href");
                t.attr("href", i), t.removeAttr("data-href"), t.off("click"), t.on("click", function() {
                    window.location = i
                })
            });
            var r = o.get(0);
            r.style.border = "1px solid transparent", setTimeout(function() {
                r.style.border = ""
            }, 0)
        }
        i.hasClass("level-2") && $(".mobile-nav").removeClass("quarternary-visible").addClass("tertiary-visible"), i.hasClass("level-3") && $(".mobile-nav").removeClass("quinary-visible").addClass("quarternary-visible")
    })
}

function generateLinks(e) {
    if (e.next("ul").hasClass("level-1"))
        var t = '<li class="js-generated back"><a href="/" class="nav-back">View Our Hotels!</a></li>';
    else var t = '<li class="js-generated back"><a href="#" class="nav-back">Back</a></li>';
    var i = window.location.pathname;
    e.attr("data-href").toLowerCase() === i.toLowerCase() ? clone = $('<li role="menu-item" class="active"><a class="active" href="' + e.attr("data-href") + '">' + e.text() + "</a></li>") : clone = $('<li role="menu-item"><a class="" href="' + e.attr("data-href") + '">' + e.text() + "</a></li>"), "Our Hotels" != e.parent("li").attr("aria-label") && (e.hasClass("nav-no-click") && clone.on("click", function(e) {
        e.preventDefault()
    }), e.next(".treelevel").children(".js-generated").length <= 0 && (e.next(".treelevel").prepend(clone), e.next(".treelevel").prepend(t)))
}

function ourHotelsHeight(e) {
    setTimeout(function() {
        $("#main-menu #our-hotels").hasClass("js-dropdown-active") && ($(window).width() >= 1325 ? $("#main-menu #our-hotels").css("height", $(".top-bar .global-nav-wrapper").height()) : $("#main-menu #our-hotels").css("height", ""))
    }, e)
}

function ftrEmail() {
    var e = $(".footer #footer-address p");
    $(e).each(function() {
        $(this).height() >= 89 ? $(this).find("span.pipe").hide() : $(this).find("span.pipe").show()
    })
}

function intervals(e, t) {
    var i = moment(e, "hh:mm a"),
        a = moment(t, "hh:mm a");
    i.minutes(15 * Math.ceil(i.minutes() / 15));
    for (var n = [], o = moment(i); o <= a;) n.push(o.format("h:mma")), o.add(15, "minutes");
    return n
}
$(document).ready(function() {
        function e() {
            if ($(".sidebar-brand > ul").mobilenav(), $(".sidebar-property > ul").mobilenav(), $(".mobile-nav > .menu > .moved").removeClass("moved"), !$("body").hasClass("brand"))
                if ($(".global-nav-wrapper a[data-active-page]").length > 0) {
                    var e = $(".global-nav-wrapper a[data-active-page]");
                    e.attr("data-href") === e.parents("ul[data-current-homepage]").attr("data-current-homepage") ? e.parents("ul[data-current-homepage]").siblings("a").trigger("move.nav").parents("li").addClass("moved") : e.parents(".has-dropdown").find("> a").trigger("move.nav").parents("li").addClass("moved")
                } else $(".global-nav-wrapper a[data-active-property]").length > 0 && $(".global-nav-wrapper a[data-active-property]").trigger("move.nav").parents("li").addClass("moved");
            $(".mobile-nav > .menu > .moved > a").each(function() {
                generateLinks($(this))
            }), navback()
        }
        $(".nav-no-click").on("click", function(e) {
            e.preventDefault()
        }), $(".lang-bar > a").on("click", function(e) {
            e.preventDefault(), $(this).parent().toggleClass("show-langs")
        });
        var t, i = !1;
        $(window).on("resize", function() {
            !1 !== i && clearTimeout(i), i = setTimeout(function() {
                var i = window.innerWidth;
                t && t >= 1340 && i < 1340 ? e() : i >= 1340 && e(), t = i
            }, 300)
        }).on("load", e), $("#tertiary-select").on("change", function() {
            this.value && this.value !== window.location.pathname && (window.location.href = this.value)
        }), $(".menu-icon").on("click", function(t) {
            t.preventDefault();
            var i = $(this);
            i.hasClass("moved") ? (i.removeClass("moved"), e()) : i.addClass("moved")
        }), $(".cd-secondary-nav .right .lang > a").on("click", function(e) {
            e.preventDefault();
            var t = $(this);
            t.toggleClass("active"), t.parent().find(".langs").slideToggle()
        }), $(".header-area .cd-auto-hide-header .cd-primary-nav .lang > a").on("click", function(e) {
            e.preventDefault();
            var t = $(this);
            t.toggleClass("active"), t.parent().find(".langs").slideToggle()
        }), $("#main-menu").on("toggle.zf.trigger", function() {
            window.innerWidth < 1340 && ($(this).is(":visible") ? $("body").css("overflow", "hidden") : $("body").css("overflow", ""))
        })
    }), $.fn.mobilenav = function() {
        var e = $(this);
        return this.each(function() {
            var t = "";
            e.hasClass("no-panel-nav") || (t = new Foundation.DropdownMenu(e)), $(".cd-secondary-nav").hasClass(".menu-active") || $("body").css("overflow", ""), $(window).width() < 1340 ? ($(".js-generated").removeClass("hide"), "" !== t ? (t.destroy(), $(".sidebar-brand").each(function() {
                $(this).addClass("mobile-nav")
            }), $(".sidebar-brand ul").each(function() {
                $(this).find("ul").each(function() {
                    var e = $(this);
                    !e.hasClass("region") && !e.parent(".region").length > 0 && e.addClass("treelevel level-" + e.parents(".treelevel").length), e.parent("li").addClass("has-dropdown")
                })
            }), $(".sidebar-brand .has-dropdown").each(function() {
                var e = $(this).find("> a"),
                    t = e.attr("href");
                e.attr("data-href", t).removeAttr("href")
            }), $(".has-dropdown > a").off("click"), $(".has-dropdown > a").on("click move.nav", function(e) {
                e.preventDefault(), $(".mobile-nav").addClass("secondary-visible"), document.getElementById("main-menu").scrollTop = 0;
                var t = $(this);
                t.parents(".region").length > 0 && ($(".mobile-nav").removeClass("secondary-visible").addClass("tertiary-visible"), $(".region").addClass("inactive").removeClass("active"), t.parents(".region").removeClass("inactive").addClass("active")), t.parents(".level-0").length > 0 && $(".mobile-nav").removeClass("secondary-visible").addClass("tertiary-visible"), t.parents(".level-1").length > 0 && $(".mobile-nav").removeClass("tertiary-visible").addClass("quarternary-visible"), t.parents(".level-2").length > 0 && $(".mobile-nav").removeClass("quarternary-visible").addClass("quinary-visible"), t.parent().addClass("moved"), navback()
            })) : ($(".dropdown-trigger").off("click"), $(".dropdown-trigger").on("click", function(e) {
                e.preventDefault();
                var t = $(this),
                    i = $(this).find("span");
                t.hasClass("active") ? (t.removeClass("active"), i.text(i.attr("data-initial")), t.parent().find(".regions").slideUp("fast")) : (t.addClass("active"), i.text(i.attr("data-active")), t.parent().find(".regions").slideDown("fast"))
            }), $(".expand-nav, .nav-languages > a").off("click"), $(".expand-nav, .nav-languages > a").on("click", function(e) {
                e.preventDefault();
                var t = $(this);
                t.parent().hasClass("expanded") ? (t.parent().removeClass("expanded"), t.parent().find("> ul").slideUp("fast")) : (t.parent().addClass("expanded"), t.parent().find("> ul").slideDown("fast"))
            }), $(".sidebar-property li.active").each(function(e) {
                var t = $(this);
                t.closest("li").length && t.parents("li").has("> .expand-nav").addClass("expanded").find("> ul").slideDown("fast")
            }))) : ($(".sidebar-brand").removeClass("mobile-nav secondary-visible tertiary-visible quarternary-visible"), $(".sidebar-brand ul").each(function() {
                $(this).find("ul").each(function() {
                    $(this).removeClass("treelevel"), $(this).parent("li").removeClass("has-dropdown")
                })
            }), $(".region").removeClass("active inactive"))
        })
    }, $.oops = function() {
        started = !1, count = 0
    }, $(function() {
        ! function(e) {
            e(".property-gallery-launch, .brand-gallery-launch, .gallery-launch").on("click", function(t) {
                t.preventDefault();
                var i = e(this),
                    a = "/ajax/gallery",
                    n = [],
                    o = i.attr("data-property-id"),
                    r = i.attr("data-category"),
                    s = i.attr("data-starting-index"),
                    l = i.attr("data-gallery-id"),
                    c = i.attr("data-page-id"),
                    h = i.attr("data-page-url"),
                    d = {
                        galleryName: "maingallery",
                        showInfo: !0,
                        showTiles: !1,
                        collapsibleFooter: !0
                    };
                s = s ? parseInt(s) : 0, o && n.push("property_id=" + o), l && n.push("gallery_id=" + l), c && n.push("page_id=" + c), h && n.push("page_url=" + h);
                var u = e("body").attr("data-language-id");
                u && n.push("language=" + u), n.length && (a += "?" + n.join("&")), d.dataSource = a, d.startingIndex = s, r && (d.startCategory = r), new Ballyhoo(e(".media-gallery"), d).launch()
            });
            var t = [];
            t = e("body").children();
            var i = function(t) {
                return Math.max.apply(null, e(t).map(function() {
                    var t;
                    return isNaN(t = parseInt(e(this).css("z-index"), 10)) ? 0 : t
                }))
            }(t);
            e(".media-gallery").css("z-index", i + 1)
        }(jQuery), $(window).on("load", function() {
            -1 != location.href.indexOf("#g") && $(".header-area .property-gallery-launch").trigger("click")
        }), $('select[name="restaurant_id"]').on("change", function() {
            var e = $("option:selected", this).attr("data-hotel");
            $('#dining-form input[name="hotel"]').val(e)
        });
        var e = '<span>Prev</span><svg xmlns="http://www.w3.org/2000/svg" width="13" height="25" viewBox="0 0 13.661 25"><path d="M13.66 12.49L1.217 0 0 .302 12.153 12.5 0 24.698 1.216 25 13.66 12.51l-.04-.01"></path></svg>',
            t = '<span>Next</span><svg xmlns="http://www.w3.org/2000/svg" width="13" height="25" viewBox="0 0 13.661 25"><path d="M13.66 12.49L1.217 0 0 .302 12.153 12.5 0 24.698 1.216 25 13.66 12.51l-.04-.01"></path></svg>';
        $(document).foundation();
        var i = new Date,
            a = (new Date, moment());
        $("#datefield .placeholder").each(function() {
            var e = a.format("dddd").substring(0, 3),
                t = a.format("MMMM"),
                i = a.format("D");
            $(this).html(e + ", " + t + " " + i)
        }), $(".res-date").each(function() {
            var i = $(this);
            i.datepick({
                dateFormat: "mm/dd/yy",
                minDate: new Date,
                showOnFocus: !0,
                monthsToShow: 1,
                changeMonth: !1,
                todayText: "",
                showOptions: {
                    direction: "up"
                },
                prevText: e,
                nextText: t,
                todayText: '<span class="hide">Today\'s month</span>',
                defaultDate: new Date,
                selectDefaultDate: !0,
                popupContainer: i.parent(),
                showAnim: "fadeIn",
                onSelect: function(e) {
                    var t = $.datepick.formatDate(e[0]);
                    i.closest("form").find('input[name="startDate"]').attr("value", t);
                    var a = moment(e[0]);
                    i.closest("form").find(".datefield .placeholder").each(function() {
                        var e = a.format("dddd").substring(0, 3),
                            t = a.format("MMMM"),
                            i = a.format("D");
                        $(this).html(e + ", " + t + " " + i)
                    })
                },
                onClose: function(e) {},
                onShow: function(e, t) {
                    e.find(".datepick-cmd-clear, .datepick-cmd-close").addClass("button button-primary"), setTimeout(function() {
                        i.hasClass("ot-push-date") ? $(".datepick-popup").css({
                            "z-index": 9,
                            top: -215,
                            left: 0
                        }) : i.hasClass("ot-picker") ? $(window).width() < 768 ? $(".datepick-popup").css({
                            top: -98
                        }) : $(".datepick-popup").css({
                            "z-index": 9,
                            top: -215,
                            left: 0
                        }) : $(window).width() < 768 ? $(".datepick-popup").css({
                            top: -45,
                            left: -76
                        }) : $(".datepick-popup").css({
                            "z-index": 9,
                            top: -55,
                            left: -60
                        }), $(".datepick-popup").find(".datepick-cmd-prev").focus()
                    }, 100)
                }
            })
        }), $(".has-date").each(function() {
            var a = $(this),
                n = a.attr("data-date-format");
            n = n || "DD, MM, yyyy", a.datepick({
                dateFormat: n,
                minDate: i,
                showOnFocus: !0,
                monthsToShow: 1,
                changeMonth: !1,
                todayText: "",
                showAnim: "drop",
                showOptions: {
                    direction: "up"
                },
                prevText: e,
                nextText: t,
                defaultDate: "",
                selectDefaultDate: !0
            })
        }), $(".datepick-popup").on("blur", function(e) {
            $(".res-date").datepick("hide")
        }), $(".dining .dining-intro header").width() > 225 && $(".dining .dining-intro .open-table").addClass("fullwidth"), $(".dining-interior .dining-intro header").width() > 225 && $(".dining-interior .dining-intro .interior-console").addClass("fullwidth"), $(".events-push .info h2 .date").each(function() {
            var e = $(this),
                t = moment(e.html());
            if (t.isValid()) {
                var i = t.format("DD MMMM YYYY");
                e.html(i)
            }
        });
        for (var n = $(".footer-right ul > li"), o = 0; o < n.length; o += 2) n.slice(o, o + 2).wrapAll('<div class="row"></div>');
        n.addClass("columns small-6"), $(".nav-section.property .menu.show-for-xxlarge .top-drop .subnav .row").each(function() {
            var e = $(this).find("li");
            if (e.length < 1) $(this).parent(".subnav").hide();
            else if (e.length <= 4)
                for (var t = 0; t < e.length; t += 4) e.slice(t, t + 4).wrapAll('<div class="columns"></div>');
            else if (e.length > 4 && e.length <= 8)
                for (var t = 0; t < e.length; t += 4) e.slice(t, t + 4).wrapAll('<div class="columns medium-6"></div>');
            else
                for (var t = 0; t < e.length; t += 4) e.slice(t, t + 4).wrapAll('<div class="columns medium-4"></div>')
        }), $(".nav-section.property .menu.show-for-xxlarge .top-drop").each(function() {
            switch ($(this).find(".columns:not(.subnav)").length) {
                case 1:
                    $(this).addClass("one-col");
                    break;
                case 2:
                    $(this).addClass("two-col");
                    break;
                case 3:
                    $(this).addClass("three-col");
                    break;
                case 4:
                    $(this).addClass("four-col");
                    break;
                default:
                    $(this).addClass("four-col")
            }
        }), $(window).trigger("resize"), $("#booking-modal-wrapper").on("toggle.zf.trigger", function(e, t) {
            if ($(".cd-auto-hide-header").hasClass("nav-is-hidden") || $(".cd-secondary-nav").hasClass("scrolled") ? $("#booking-modal-wrapper").css("top", "60px") : $("#booking-modal-wrapper").css("top", "85px"), t.hasClass("revealed")) {
                var i;
                $(window).on("scroll.booking-modal", function() {
                    i && clearTimeout(i), i = window.setTimeout(function() {
                        $(".cd-auto-hide-header").hasClass("nav-is-hidden") || $(".cd-secondary-nav").hasClass("scrolled") ? $("#booking-modal-wrapper").css("top", "60px") : $("#booking-modal-wrapper").css("top", "85px")
                    }, 100)
                })
            } else $(window).off("scroll.booking-modal")
        }), $(document).on("show.zf.dropdownmenu", function(e, t) {
            "our-hotels" == t.attr("id") && ourHotelsHeight(0)
        }), $("#main-menu").on("mouseleave", function() {
            $(".map-trigger").removeClass("active"), $("#map-canvas").empty()
        }), $("#main-menu .global-nav-wrapper").on("click", function(e) {
            e.target === this && $(".menu-icon.moved").trigger("click")
        }), $(".menu-icon").not(".moved").on("click", function(e) {
            e.preventDefault(), $('[aria-label="Our Hotels"] > a:first-child').focus()
        }), $(".best-rate > a").on("click", function(e) {
            e.preventDefault();
            var t = $(this).parent(),
                i = t.find(".best-rate-message"),
                a = new TimelineLite;
            i.hasClass("active") ? a.to(i, .25, {
                ease: Power4.easeOut,
                opacity: 0,
                y: 30,
                onComplete: function() {
                    i.removeClass("active")
                },
                clearProps: "all"
            }) : a.to(i, .25, {
                ease: Power4.easeIn,
                opacity: 1,
                y: -30,
                onStart: function() {
                    i.addClass("active")
                }
            })
        }), $(".best-rate-close").on("click", function(e) {
            e.preventDefault();
            var t = $(this).parent();
            (new TimelineLite).to(t, .25, {
                ease: Power4.easeOut,
                opacity: 0,
                y: -30,
                onComplete: function() {
                    t.removeClass("active")
                },
                clearProps: "all"
            })
        });
        var r, s = $(window),
            l = $(".cd-auto-hide-header"),
            c = $(".cd-primary-nav"),
            h = $(".cd-secondary-nav"),
            d = $(".content-area-main").first();
        $(".brand-console-ctas .search").length && $(".booking-console .console .submit").length && (d = $("body").hasClass("brand") ? $(".brand-console-ctas .search") : $(".booking-console .console .submit"));
        var u = [d],
            p = [c, h];
        if (s.on("scroll", function() {
                r && clearTimeout(r), r = setTimeout(function() {
                    var e = parseInt(s.scrollTop()),
                        t = (parseInt(s.height()), 0);
                    $(".booking-console .console .submit").is(":visible") ? l.removeClass("no-console") : l.addClass("no-console"), u.forEach(function(i) {
                        if (d.length && (t += parseInt(d.offset().top)), p.forEach(function(e) {
                                e.length && e.each(function() {
                                    $(this).is(":visible") && (t -= parseInt($(this).height()))
                                })
                            }), window.innerWidth >= 1024) {
                            var a = $(".booking-console .form-button").height();
                            t += a || 0
                        }
                        0 !== e && e >= t ? l.addClass("show-book") : l.removeClass("show-book")
                    })
                }, 20)
            }), ftrEmail(), $(window).resize(function() {
                ftrEmail(), ourHotelsHeight(0)
            }), $("#country").on("change", function() {
                var e = $(this).find(":selected").attr("data-show"),
                    t = $("#state");
                switch (e) {
                    case "us":
                        t.attr("class", "show-us").removeAttr("disabled");
                        break;
                    case "ca":
                        t.attr("class", "show-ca").removeAttr("disabled");
                        break;
                    case "mx":
                        t.attr("class", "show-mx").removeAttr("disabled");
                        break;
                    case "au":
                        t.attr("class", "show-au").removeAttr("disabled");
                        break;
                    case "cn":
                        t.attr("class", "show-cn").removeAttr("disabled");
                        break;
                    default:
                        t.removeAttr("class").attr("disabled", "true")
                }
                t[0].selectedIndex = 0
            }), $(document).on("transitionend webkitTransitionEnd oTransitionEnd", "#our-hotels, .js-dropdown-active", function() {
                $(".global-nav-wrapper").css("overflow-y", "")
            }), $(window).width() < 1024 && ($(document).on("touchend", $.datepick._checkExternalClick), $(window).scroll(function() {
                $(".res-date").blur()
            }), navigator.userAgent.toLowerCase().indexOf("android") > -1 && $(window).scroll(function() {
                $(".res-date").datepick("hide").blur()
            })), $("#RestaurantID").on("change", function() {
                $("#ResTime").html("");
                var e = $(this).find("option:selected").data("start"),
                    t = $(this).find("option:selected").data("end");
                if ("" !== e && "" !== t) {
                    var i = intervals(e, t);
                    $.each(i, function(e, t) {
                        $("#ResTime").append('<option value="' + t + '">' + t + "</option>")
                    })
                } else i = intervals("17:30:00", "21:30:00"), $.each(i, function(e, t) {
                    $("#ResTime").append('<option value="' + t + '">' + t + "</option>")
                })
            }), $("#ResTime").length > 0) {
            var f = $("#ResTime"),
                g = f.data("start"),
                m = f.data("end");
            if ("" !== g && "" !== m) {
                f.html("");
                var v = intervals(g, m);
                $.each(v, function(e, t) {
                    f.append('<option value="' + t + '">' + t + "</option>")
                })
            } else f.html(""), v = intervals("17:30:00", "21:30:00"), $.each(v, function(e, t) {
                $("#ResTime").append('<option value="' + t + '">' + t + "</option>")
            })
        }
    }), window._MO = window._MO || {}, _MO.videoTracking = {
        enable: function(e) {
            var t = {
                25: !1,
                50: !1,
                75: !1
            };
            e.on("playing", function() {
                var e = this,
                    i = e.currentSrc().split("/").pop();
                i && (i = i.replace(/\..*/, ""));
                for (var a in t) t[a] = !1;
                _trackData && _trackData({
                    video: {
                        name: i,
                        length: parseInt(e.duration())
                    },
                    events: {
                        videoStart: !0
                    },
                    page: {
                        url: window.location.href
                    }
                })
            }).on("ended", function() {
                var e = this,
                    t = e.currentSrc().split("/").pop();
                t && (t = t.replace(/\..*/, "")), _trackData && _trackData({
                    video: {
                        name: t,
                        length: parseInt(e.duration()),
                        milestone: {
                            ID: "100"
                        }
                    },
                    events: {
                        videoComplete: !0
                    },
                    page: {
                        url: window.location.href
                    }
                })
            }).on("progress", function() {
                var e, i, a, n, o, r, s, l = this;
                e = l.currentSrc().split("/").pop(), e && (e = e.replace(/\..*/, "")), i = parseInt(l.duration()), a = parseInt(l.currentTime()), o = parseInt(.25 * i), r = parseInt(.5 * i), s = parseInt(.75 * i), a >= o && a < r && (n = "25"), a >= r && a < s && (n = "50"), a >= s && (n = "75"), n && !t[n] && _trackData && (_trackData({
                    video: {
                        name: e,
                        length: i,
                        milestone: {
                            ID: n
                        }
                    },
                    events: {
                        videoMilestone: !0
                    },
                    page: {
                        url: window.location.href
                    }
                }), t[n] = !0)
            })
        }
    }, $(document).ready(function() {
        var e = {
            Android: function() {
                return navigator.userAgent.match(/Android/i)
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i)
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i)
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i)
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i)
            },
            any: function() {
                return e.Android() || e.BlackBerry() || e.iOS() || e.Opera() || e.Windows()
            }
        };
        document.documentElement.setAttribute("data-useragent", navigator.userAgent), navigator.appVersion, $(".map-outer-div-experimental").each(function() {
            var t = $(this),
                i = t.attr("data-latitude"),
                a = t.attr("data-longitude");
            if (e.any()) {
                if (e.Android()) var n = "geo:" + i + "," + a;
                else if (e.iOS())
                    if (/(iPhone|iPad|iPod)\sOS\s6/.test(navigator.userAgent)) var n = "maps://?q=" + i + "," + a;
                    else var n = "comgooglemaps://?q=" + i + "," + a;
                else if (e.Windows()) var n = "maps://?q=" + i + "," + a;
                t.append('<section class="map-links"><a href="' + n + '" data-rel="external">Launch Your Maps App</a></section>')
            }
        })
    }), $(function() {
        function e() {
            if ($(".offers-two.current-offer").length) {
                var e = $(".offers-two.current-offer").find(".long-description").height() + 35;
                $(".offers-two.current-offer").css("margin-bottom", e)
            }
            var t = [];
            $(".offers-two header").each(function() {
                $(this).removeAttr("style"), t.push($(this).height());
                var e = Math.max.apply(null, t);
                $(this).height(e)
            })
        }

        function t() {
            $(".copy-link-input").parent().toggleClass("active")
        }
        $(".promo-trigger").on("click", function(e) {
            e.preventDefault();
            var t = jQuery(this);
            t.hasClass("active") ? (t.removeClass("active"), t.find(".fa").removeClass("fa-caret-up").addClass("fa-caret-down"), t.next(".flyout-wrapper").slideUp("fast")) : (t.addClass("active"), t.find(".fa").removeClass("fa-caret-down").addClass("fa-caret-up"), t.next(".flyout-wrapper").slideDown("fast").css("overflow", "visible"))
        }), $(".promo-close").on("click", function(e) {
            e.preventDefault(), $(".promo-trigger").click()
        }), $(".form-heading h2").on("click", function(e) {
            e.preventDefault();
            var t = jQuery(this);
            t.parent().hasClass("active") ? (t.find(".fa").removeClass("fa-angle-up").addClass("fa-angle-down"), t.parent().removeClass("active").next(".dropdown").slideUp()) : (t.find(".fa").removeClass("fa-angle-down").addClass("fa-angle-up"), t.parent().addClass("active").next(".dropdown").slideDown())
        }), $(window).on("resize load", function() {
            window.innerWidth > 641 ? ($(".horizontal-console .dropdown").removeAttr("style"), $(".console").each(function() {})) : $(".console").each(function() {})
        }), $.support.placeholder = "placeholder" in document.createElement("input"), $("form").each(function() {
            $.support.placeholder || $(this).addClass("no-placeholder")
        }), $(".category-filter").on("click", "a", function(e) {
            e.preventDefault();
            var t = $(this),
                i = t.find(".toggle-name");
            t.parent().hasClass("active") ? (t.parent().removeClass("active"), t.find(".fa").removeClass("fa-check-square-o").addClass("fa-square-o"), i.text(i.data("toggle-off"))) : (t.parent().addClass("active"), t.find(".fa").removeClass("fa-square-o").addClass("fa-check-square-o"), i.text(i.data("toggle-on")));
            var a = [];
            $(".category-filter.active a").each(function() {
                a.push($(this).attr("data-category"))
            }), $(".offers-listing li").hide(), $(".offers-listing").each(function() {
                listing = $(this), listing.children("li").each(function() {
                    for (var e = $(this), t = e.attr("data-category") ? e.attr("data-category").split(" ") : [], i = t.length - 1; i >= 0; i--) a.indexOf(t[i]) >= 0 && e.show()
                })
            }), 0 === $(".offers-listing li:visible").length ? $(".offers-listing").find(".message").css("display", "block") : $(".offers-listing").find(".message").css("display", "none")
        }), $(".expand-toggle").on("click", function(e) {
            e.preventDefault();
            var t = $(this);
            t.parent().hasClass("active") ? (t.parent().removeClass("active"), t.text(t.data("toggle-open")), t.parent().siblings(".expandable").slideUp(function() {
                $(t.parent().siblings(".expandable-intro")).length > 0 && $(t.parent().siblings(".expandable-intro")).hide()
            })) : (t.parent().addClass("active"), t.text(t.data("toggle-close")), t.parent().siblings(".expandable").slideDown(), $(t.parent().siblings(".expandable-intro")).length > 0 && $(t.parent().siblings(".expandable-intro")).show()), $(this).hasClass("bordered-transparent") && $(this).parents(".info").length > 0 && $(this).parents(".info").children("header").each(function() {
                $(this).find(".intro").toggle(), $(this).find("p:last-child").toggleClass("hide")
            }), $(this).hasClass("bordered-white") && $(this).parents(".info").find(".info-button").toggleClass("hide")
        }), $(".expand-single").on("click", function(e) {
            e.preventDefault();
            var t = $(this);
            $(".expand-single").each(function() {
                $(this).text(t.data("toggle-open")), t.parent().hasClass("active") ? $(this).parent().siblings(".expandable").slideUp() : $(this).parent().siblings(".expandable").hide()
            }), t.parent().hasClass("active") ? (t.parent().removeClass("active"), t.text(t.data("toggle-open")), t.parent().siblings(".expandable").slideUp()) : ($(".expand-single").each(function() {
                $(this).parent().removeClass("active")
            }), t.parent().addClass("active"), t.text(t.data("toggle-close")), t.parent().siblings(".expandable").slideDown({
                start: function() {
                    t.parents(".offers-two").length && $(".offers-two").css("margin-bottom", "0")
                },
                step: function() {
                    if (t.parents(".offers-two").length) {
                        var e = $(this).height() + 35;
                        t.parents(".offers-two").css("margin-bottom", e)
                    }
                }
            })), t.parents(".offers-two").length && ($(".offers-two").not(t.parents(".offers-two")).each(function() {
                $(this).removeClass("current-offer")
            }), t.parents(".offers-two.current-offer").animate({
                marginBottom: "0px"
            }), t.parents(".offers-two").toggleClass("current-offer"))
        });
        var i = !1;
        $(window).bind("resize load", function() {
            !1 !== i && clearTimeout(i), i = setTimeout(e, 100)
        }), $(".offers-two h3, .offers-two h4").each(function() {
            if ($(this).text().length > 40) {
                var e = $(this).text().substr(0, 40),
                    t = e.substr(0, e.lastIndexOf(" ")) + "...";
                $(this).text(t)
            }
        }), $(".info-button").on("click", function() {
            $(this).unbind("onmouseover").unbind("onmouseout"), $(this).removeClass("hover")
        }), $(".info-button").hover(function() {
            $(this).addClass("hover")
        }, function() {
            $(this).removeClass("hover")
        }), $(".next-sibling-toggle a").first().click(function(e) {
            e.preventDefault(), $(this).toggleClass("activated"), $(this).parents(".next-sibling-toggle").children(".sibling-togglable").toggleClass("hidden")
        }), $(".next-sibling-toggle h2 span").click(function(e) {
            $(".next-sibling-toggle a").click()
        }), $("#open-advanced").click(function(e) {
            e.preventDefault(), $(this).toggleClass("activated"), $("#advanced").toggle()
        }), $(".top").click(function(e) {
            e.preventDefault(), $(this).parents(".roll-up").toggleClass("activated")
        }), $(".info .toggle-listing").on("click", function(e) {
            e.preventDefault(), $(this).parents(".roll-up").toggleClass("activated")
        }), $(".share-input").click(function() {
            $(this).parent().toggleClass("active")
        }), $(".image-sharing").on("click", ".fi-link", function(e) {
            e.preventDefault();
            var t = $(this).parent().find(".copy-link"),
                i = $(this).parent().find(".copy-link-input");
            t.hasClass("active") ? t.removeClass("active") : (t.addClass("active"), i.select())
        }), $(".copy-link-input").bind("copy", function(e) {
            setTimeout(t, 600)
        }), $(".contact-us, .rfp").each(function() {
            var e, t, i;
            e = $("#state"), i = $("#country"), t = i.find('option:contains("United States of America")'), e && e.on("change", function() {
                $(this).attr("required", "required"), $(this).prev("label").html('<span class="alert">*</span> State'), t.prop("selected", "selected")
            }), i && i.on("change", function() {
                "United States" != $(this).val() ? (e.removeAttr("required"), e.prev("label").html("State"), e.find("option:first-child").prop("selected", "selected")) : (e.prev("label").html('<span class="alert">*</span> State'), e.attr("required", "required"))
            })
        }), $(".rooms-filter select").on("change", function() {
            var e = $(this).find("option:selected").attr("data-href");
            $('.rooms-filter ul.tabs a[href="' + e + '"]').click()
        }), $(".rooms-filter ul.tabs a").on("click", function() {
            var e = $(this).attr("href");
            $('.rooms-filter select option[data-href="' + e + '"]').prop({
                selected: !0
            })
        }), $(".tab-filter select").on("change", function() {
            var e = $(this).find("option:selected").attr("data-href");
            $('.tab-filter ul.tabs a[href="' + e + '"]').click()
        }), $(".tab-filter ul li:first-child").addClass("is-active"), $(".property-tabbed .tabs-panel:first-of-type").addClass("is-active")
    }), $(function() {
        $(".celebrity > a, .social-grid > .button").on("click", function(e) {
            e.preventDefault()
        })
    }), $(function() {
        function e() {
            var e = $(window).height() - 70;
            e = parseInt(e) + "px", $(".brand #search-hotels .search-map, .brand #search-hotels .mapDiv").css("height", e)
        }
        var t, i = !1,
            a = !1,
            n = function() {
                $(".search-map .map-outer-div").each(function() {
                    function e(e) {
                        t || (t = e);
                        var i = $(".search-map .map-outer-div").data("latitude"),
                            a = $(".search-map .map-outer-div").data("longitude"),
                            r = new google.maps.LatLng(i, a),
                            s = {
                                center: r,
                                zoom: 2,
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                mapTypeControl: !0,
                                panControl: !0,
                                zoomControl: !0,
                                streetViewControl: !1,
                                styles: [{
                                    elementType: "geometry",
                                    stylers: [{
                                        color: "#dadad0"
                                    }]
                                }, {
                                    elementType: "geometry.fill",
                                    stylers: [{
                                        color: "#dadad0"
                                    }]
                                }, {
                                    elementType: "geometry.stroke",
                                    stylers: [{
                                        color: "#585857"
                                    }]
                                }, {
                                    elementType: "labels",
                                    stylers: [{
                                        visibility: "off"
                                    }]
                                }, {
                                    elementType: "labels.text",
                                    stylers: [{
                                        color: "#585857"
                                    }, {
                                        weight: 2
                                    }]
                                }, {
                                    elementType: "labels.text.fill",
                                    stylers: [{
                                        color: "#616161"
                                    }]
                                }, {
                                    elementType: "labels.text.stroke",
                                    stylers: [{
                                        color: "#f5f5f5"
                                    }]
                                }, {
                                    featureType: "administrative.land_parcel",
                                    stylers: [{
                                        visibility: "off"
                                    }]
                                }, {
                                    featureType: "administrative.neighborhood",
                                    stylers: [{
                                        visibility: "off"
                                    }]
                                }, {
                                    featureType: "administrative.province",
                                    elementType: "labels.text",
                                    stylers: [{
                                        color: "#777777"
                                    }, {
                                        visibility: "off"
                                    }, {
                                        weight: .5
                                    }]
                                }, {
                                    featureType: "administrative.province",
                                    elementType: "labels.text.stroke",
                                    stylers: [{
                                        color: "#ffffff"
                                    }]
                                }, {
                                    featureType: "poi",
                                    elementType: "geometry",
                                    stylers: [{
                                        color: "#eeeeee"
                                    }]
                                }, {
                                    featureType: "poi",
                                    elementType: "labels.text.fill",
                                    stylers: [{
                                        color: "#757575"
                                    }]
                                }, {
                                    featureType: "poi.park",
                                    elementType: "geometry",
                                    stylers: [{
                                        color: "#e5e5e5"
                                    }]
                                }, {
                                    featureType: "poi.park",
                                    elementType: "labels.text.fill",
                                    stylers: [{
                                        color: "#9e9e9e"
                                    }]
                                }, {
                                    featureType: "road",
                                    elementType: "geometry",
                                    stylers: [{
                                        color: "#ffffff"
                                    }]
                                }, {
                                    featureType: "road.arterial",
                                    elementType: "labels.text.fill",
                                    stylers: [{
                                        color: "#757575"
                                    }]
                                }, {
                                    featureType: "road.highway",
                                    elementType: "geometry",
                                    stylers: [{
                                        color: "#dadada"
                                    }]
                                }, {
                                    featureType: "road.highway",
                                    elementType: "labels.text.fill",
                                    stylers: [{
                                        color: "#616161"
                                    }]
                                }, {
                                    featureType: "road.local",
                                    elementType: "labels.text.fill",
                                    stylers: [{
                                        color: "#9e9e9e"
                                    }]
                                }, {
                                    featureType: "transit.line",
                                    elementType: "geometry",
                                    stylers: [{
                                        color: "#e5e5e5"
                                    }]
                                }, {
                                    featureType: "transit.station",
                                    elementType: "geometry",
                                    stylers: [{
                                        color: "#eeeeee"
                                    }]
                                }, {
                                    featureType: "water",
                                    elementType: "geometry",
                                    stylers: [{
                                        color: "#ffffff"
                                    }]
                                }, {
                                    featureType: "water",
                                    elementType: "labels.text.fill",
                                    stylers: [{
                                        color: "#9e9e9e"
                                    }]
                                }]
                            },
                            l = new google.maps.Map(document.getElementById("map-canvas-nav"), s);
                        map_search = new google.maps.Map(document.getElementById("map-canvas-search"), s), l.panBy(0, 128), map_search.panBy(0, 256), $(".regions-wrapper").append('<div class="loader"><span></span><span></span><span></span></div>'), $(".regions-wrapper .loader").remove(), $.each(e, function() {
                            for (var t = [], i = 0; i < e.data.record.length; i++) {
                                var a = e.data.record[i];
                                if ("1" != a.notbookable || a.latitude && a.longitude) {
                                    var r = {
                                        name: a.name,
                                        slug: a.slug,
                                        latitude: a.latitude,
                                        longitude: a.longitude,
                                        image: a.image,
                                        image_alt: a.image_alt,
                                        city: a.city,
                                        currency: a.currency,
                                        country: a.country,
                                        region: a.region,
                                        region_id: a.region_id,
                                        short_description: a.short_description,
                                        rate_from: a.rate_from,
                                        pinW: 21,
                                        pinH: 30,
                                        anchorX: 10,
                                        anchorY: 30,
                                        content_string: '<div class="row ib" data-equalizer="">' + (a.image ? '<div class="columns large-5 image" data-equalizer-watch=""><img src="' + a.image + '" alt="' + a.image_alt + '" /></div>' : "") + '<div class="columns ' + (a.image ? "large-7 " : "") + 'info align-self-top" data-equalizer-watch=""><section><div class="inner">' + (void 0 !== a.rate_from ? '<span style="color: ' + a.color + ';" class="rates-from">Rates From ' + a.currency + " " + a.rate_from + "</span>" : "") + (void 0 !== a.name ? "<h1>" + a.name + "</h1>" : "") + (null !== a.short_description ? "<p>" + a.short_description + "</p>" : "") + '<a href="#" class="close">&times;</a></div></section><a style="background-color: ' + a.color + ';" href="/' + a.homepage + '">Start Exploring</a></div></div>',
                                        homepage: a.homepage
                                    };
                                    "1" == a.underdevelopment && (r.latitude = null, r.longitude = null), t.push(r)
                                }
                            }
                            $(".search-trigger").autocomplete({
                                source: function(e, a) {
                                    var r = new RegExp($.ui.autocomplete.escapeRegex(e.term), "i"),
                                        s = $.grep(t, function(e) {
                                            var t = e.city,
                                                i = e.country,
                                                a = e.name,
                                                n = e.region;
                                            return r.test(t) || r.test(i) || r.test(a) || r.test(n)
                                        });
                                    for (i = 0; i < o.length; i++) o[i].setMap(null);
                                    o = [], n(l, s), n(map_search, s), e.term.length < 3 && (n(l, t), n(map_search, t)), 0 == $("#our-hotels .search-list .region:visible").length ? ($("#our-hotels .search-list .no-results").remove(), $("#our-hotels .search-list").append('<p class="no-results">No results found</p>')) : $("#our-hotels .search-list .no-results").remove(), $(window).width() > 1339 ? $(".top-bar .vertical.menu .region .cities").show() : $(".top-bar .vertical.menu .region .cities").hide(), $("#search-hotels .search-list .region .cities").show(), s.length > 0 && e.term.length > 2 ? $(window).width() < 1340 && $(".global-nav-wrapper .region-heading").trigger("click") : ($(".global-nav-wrapper .region-heading").next(".cities").slideUp(), $(".global-nav-wrapper .region-heading").parent(".region").removeClass("open-region"))
                                },
                                delay: 250,
                                minLength: 0
                            }), n(l, t), n(map_search, t)
                        })
                    }

                    function n(e, t) {
                        function i(e) {
                            var t = $('.sidebar-brand .region[data-region-id="' + e.name + '"]'),
                                i = t.find(".region-heading");
                            e[0] && i.html("<span>" + e[0].region + "</span>");
                            var a = t.find(".cities");
                            a.html("");
                            for (var n = 0; n < e.length; n++) {
                                var o = '<li><a data-slug="' + e[n].slug + '" href="/' + e[n].homepage + '" tabindex="0">' + ("" != e[n].name ? "" + e[n].name : "") + "</a></li>";
                                a.append(o)
                            }
                        }

                        function a(e, t) {
                            for (var i in e)
                                if (e[i].slug == t) return e[i]
                        }

                        function n(e) {
                            $(".mapDiv").addClass("show"), $(".property-info").html(a(t, e).content_string);
                            for (var i = (new Foundation.Equalizer($(".ib")), 0); i < o.length; i++) o[i].url != e ? o[i].setIcon(d) : o[i].setIcon(u)
                        }
                        var r = [];
                        r.name = "region_3";
                        var s = [];
                        s.name = "region_1";
                        var l = [];
                        l.name = "region_7";
                        for (var c = 0; c < t.length; c++) {
                            var h = t[c];
                            if (null !== h.latitude || null != h.longitude) {
                                var d = {
                                        url: "/templates/main/img/map/pin.png",
                                        size: new google.maps.Size(h.pinW, h.pinH),
                                        origin: new google.maps.Point(0, 0),
                                        anchor: new google.maps.Point(h.anchorX, h.anchorY)
                                    },
                                    u = {
                                        url: "/templates/main/img/map/pin-active.png",
                                        size: new google.maps.Size(h.pinW, h.pinH),
                                        origin: new google.maps.Point(0, 0),
                                        anchor: new google.maps.Point(h.anchorX, h.anchorY)
                                    },
                                    p = new google.maps.Marker({
                                        position: new google.maps.LatLng(h.latitude, h.longitude),
                                        map: e,
                                        title: h.name,
                                        icon: d,
                                        url: h.slug
                                    });
                                o.push(p), h.active = !1, google.maps.event.addListener(p, "click", function() {
                                    for (var e = 0; e < o.length; e++) o[e].active = !1, o[e].setIcon(d);
                                    this.setIcon(u), n(this.url), this.active = !0
                                }), google.maps.event.addListener(p, "mouseover", function() {
                                    this.setIcon(u)
                                }), google.maps.event.addListener(p, "mouseout", function() {
                                    this.active || this.setIcon(d)
                                })
                            }
                            switch (h.region_id) {
                                case "3":
                                    r.push(h);
                                    break;
                                case "1":
                                    s.push(h);
                                    break;
                                case "7":
                                    l.push(h)
                            }
                        }! function() {
                            for (var e = 0; e < arguments.length; e++) i(arguments[e]);
                            $(".search-list .region").each(function() {
                                0 == $(this).find(".cities").children().length ? $(this).hide() : $(this).show()
                            }), 0 == $(".search-hotels .search-list .region:visible").length ? ($(".search-hotels .search-list .no-results").remove(), $(".search-hotels .search-list").append('<p class="no-results">No results found</p>')) : $(".search-hotels .search-list .no-results").remove()
                        }(r, s, l), $(document).on("click", ".ib .info .close", function(e) {
                            e.preventDefault(), $(".property-info").html("");
                            for (var t = 0; t < o.length; t++) o[t].setIcon(d)
                        })
                    }
                    var o = [],
                        r = $("body").attr("data-language-id");
                    if (r) var s = "?language=" + r;
                    var l = !1;
                    $(window).bind("resize", function() {
                            !1 !== l && clearTimeout(l), l = setTimeout(function() {
                                $(window).width() > 1339 ? $(".top-bar ul:not(.sidebar-property) .vertical.menu .region .cities").show() : $(".top-bar ul:not(.sidebar-property) .vertical.menu .region .cities").hide(), $("#search-hotels .search-list .region .cities").show(), $(".search.map-trigger").off("click"), $(".map-flyout .map-trigger").off("click"), $(window).width() < 1024 && $("#search-hotels").foundation("close")
                            }, 300)
                        }),
                        function() {
                            if (i) e(t);
                            else {
                                if (a) return;
                                a = !0, $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyAaPIsk8u6yUZ39Muc_oUCQ0myADifWunc", function() {
                                    $.getJSON("/ajax/getproperties" + s, e).done(function() {
                                        i = !0, a = !1
                                    })
                                })
                            }
                        }()
                })
            };
        e(), $(window).bind("resize", e), $("#search-hotels").on("open.zf.reveal", function() {
            $(".search-trigger").val(""), $("#search-trigger-overlay").focus(), n()
        }), $("#main-menu").on("toggle.zf.trigger", function() {
            $(".search-trigger").val(""), $(".map-flyout .mapDiv").hide(), n(), $(window).width() < 1340 && ($(".global-nav-wrapper .region-heading").next(".cities").slideUp(), $(".global-nav-wrapper .region-heading").parent(".region").removeClass("open-region"))
        }), $(document).on("click", ".nav-section.property .map-trigger", function(e) {
            e.preventDefault()
        }), $(document).on("click", ".map-flyout .map-trigger", function(e) {
            e.preventDefault(), $(this).parents("#our-hotels").hasClass("map-active") ? ($(this).parents("#our-hotels").removeClass("map-active"), $(".property-info").html(""), $(".map-flyout .mapDiv").hide(), $("#map-canvas-nav, #map-canvas-search").empty().removeAttr("style"), $(".mapDiv").children("a.close-nav-map").remove()) : ($(this).parents("#our-hotels").addClass("map-active"), $(".map-flyout .mapDiv").show(), $(".mapDiv").append('<a href="#" class="close-nav-map"><em class="alt">Close</em></a>')), $(".search-trigger").autocomplete("close").val(""), n()
        }), $(".side-drop").on("click", ".close-nav-map, .list-view", function(e) {
            e.preventDefault(), $(this).parents("#our-hotels").hasClass("map-active") && ($(this).parents("#our-hotels").removeClass("map-active"), $(".property-info").html(""), $(".map-flyout .mapDiv").hide(), $("#map-canvas-nav").empty().removeAttr("style"), $(".mapDiv").children("a.close-nav-map").remove())
        }), $("#search-hotels .close-button").on("click", function(e) {
            e.preventDefault(), $(".property-info").html(""), $(".mapDiv").removeClass("show"), $("#map-canvas-nav, #map-canvas-search").empty().removeAttr("style"), $(".search-trigger").val(""), $(".no-results").remove()
        }), $(document).on("click", ".global-nav-wrapper .region-heading", function(e) {
            $(this).parent(".region").hasClass("open-region") ? ($(this).next(".cities").slideUp(), $(this).parent(".region").removeClass("open-region")) : ($(this).next(".cities").slideDown(), $(this).parent(".region").addClass("open-region"))
        })
    }),
    function(e, t) {
        var i = function(e, i) {
            this.elem = e, this.$elem = t(e), this.options = i, this._init()
        };
        i.prototype = {
            defaults: {
                clientName: "Mandarin Oriental",
                holder: t(".media-gallery"),
                splitCategory: !0,
                dataSource: "",
                startCategory: "",
                showTiles: !0,
                splitMedia: !0,
                initialMedia: "photos",
                showInfo: !1,
                showTiles: !0,
                galleryName: "",
                startingIndex: 0,
                launchIncontext: !1,
                collapsibleFooter: !1
            },
            _init: function() {
                if (this.config = t.extend({}, this.defaults, this.options), "" === this.config.dataSource) throw new Error("Missing dataSource. Kind of need that...");
                if ("" === this.config.galleryName) throw new Error("Missing Gallery Name");
                if (e.location.hash && "" !== e.location.hash && ("g" === e.location.hash.charAt(1).toLowerCase() || "i" === e.location.hash.charAt(1).toLowerCase())) {
                    var i = _parseHash();
                    i.galleryName === this.config.galleryName && this.launch(i)
                }
                return this
            },
            _loadGallery: function(e) {
                ("object" == typeof e.holder[0] ? e.holder : t(".media-gallery")).addClass("show").find(".galleria-loader").show(), "object" == typeof e.dataSource ? t.startEngines(e, e.dataSource) : t.when(t.ajax({
                    url: e.dataSource,
                    dataType: "json"
                })).then(function(i) {
                    var a = [];
                    i && i.data && !t.isEmptyObject(i.data.record) && (a = t.isArray(i.data.record) ? i.data.record : [i.data.record], t.startEngines(e, a))
                })
            },
            parseHash: function() {
                return _parseHash()
            },
            launch: function(e) {
                var i = this.config;
                t.isEmptyObject(e) || t.each(e, function(e, t) {
                    i[e] = t
                }), this._loadGallery(i)
            }
        }, i.defaults = i.prototype.defaults, t.fn.ballyhoo = function(e) {
            return this.each(function() {
                new i(this, e)._init().launch()
            })
        }, t.getAJAXData = function(e) {
            t.getJSON(e.dataSource, function(e) {
                dataholder = e
            })
        }, t.startEngines = function(e, i) {
            var a = "object" == typeof e.holder[0] ? e.holder : t(".media-gallery"),
                n = {};
            "" !== e.startCategory ? (n = t.grep(i, function(i, a) {
                if (!t.isEmptyObject(i.category) && i.category.toLowerCase().split(",").indexOf(e.startCategory.toLowerCase()) > -1) {
                    if (!e.splitMedia) return i;
                    if (void 0 !== i.video && "videos" == e.initialMedia) return i;
                    if (void 0 === i.video && "videos" !== e.initialMedia) return i
                }
            }), e.showTiles = !1) : n = e.splitMedia ? t.grep(i, function(t, i) {
                return void 0 !== t.video && "videos" == e.initialMedia ? t : void 0 === t.video && "videos" !== e.initialMedia ? t : void 0
            }) : i;
            var o = t.grep(i, function(e, t) {
                if (e.video) return !0
            });
            a.addClass("show").find(".galleria").galleria({
                dataSource: n,
                debug: !1,
                show: parseInt(e.startingIndex),
                sabreConfig: {
                    galleryName: e.galleryName,
                    clientName: e.clientName,
                    holder: e.holder,
                    splitCategory: e.splitCategory,
                    startCategory: e.startCategory,
                    splitMedia: o,
                    initialMedia: e.initialMedia,
                    showInfo: e.showInfo,
                    showTiles: e.showTiles,
                    rawData: i,
                    launchIncontext: e.launchIncontext,
                    collapsibleFooter: e.collapsibleFooter
                }
            })
        }, e.Ballyhoo = i
    }(window, jQuery);
var _parseHash = function() {
    var e = {
            galleryName: "galleria",
            initialMedia: "photo",
            index: 0,
            category: "",
            launchIncontext: !1
        },
        t = window.location.hash;
    if (void 0 !== t) {
        var i, a;
        t.split("/").length - 1 - ("/" == t[t.length - 1] ? 1 : 0) < 4 ? (i = /\#g\/*(\w*)\/*(\w*)\/*(\d*)\/*(\w*)/i, a = /\#g\/*(\w*)\/*(\w*)\/*(\d*)\/*(\w*)/i) : (i = /\#g\/*(\w*)\/*(\w*)\/*(\d*)\/*([\w-_&* ]+)/i, a = /\#i\/*(\w*)\/*(\w*)\/*(\d*)\/*([\w-_&* ]+)/i);
        var n = i.exec(t);
        if ("g" == t.charAt(1).toLowerCase()) var n = i.exec(t);
        else if ("i" == t.charAt(1).toLowerCase()) {
            var n = a.exec(t);
            e.launchIncontext = !0
        }
        void 0 !== n[1] && "" !== n[1] && (e.galleryName = n[1]), void 0 !== n[2] && "" !== n[2] && (e.initialMedia = n[2]), void 0 !== n[3] && "" !== n[3] && (e.startingIndex = n[3]), void 0 !== n[4] && "" !== n[4] && (e.startCategory = n[4])
    }
    return e
};
$(document).ready(function() {}),
    function(e, t, i, a) {
        var n = t.document,
            o = e(n),
            r = e(t),
            s = Array.prototype,
            l = !0,
            c = 3e4,
            h = !1,
            d = navigator.userAgent.toLowerCase(),
            u = t.location.hash.replace(/#\//, ""),
            p = t.location.protocol,
            f = Math,
            g = function() {},
            m = function() {
                return !1
            },
            v = function() {
                var e = 3,
                    t = n.createElement("div"),
                    i = t.getElementsByTagName("i");
                do {
                    t.innerHTML = "\x3c!--[if gt IE " + ++e + "]><i></i><![endif]--\x3e"
                } while (i[0]);
                return e > 4 ? e : n.documentMode || a
            }(),
            y = function() {
                return {
                    html: n.documentElement,
                    body: n.body,
                    head: n.getElementsByTagName("head")[0],
                    title: n.title
                }
            },
            w = t.parent !== t.self,
            b = "data ready thumbnail loadstart loadfinish image play pause progress fullscreen_enter fullscreen_exit idle_enter idle_exit rescale lightbox_open lightbox_close lightbox_image",
            x = function() {
                var t = [];
                return e.each(b.split(" "), function(e, i) {
                    t.push(i), /_/.test(i) && t.push(i.replace(/_/g, ""))
                }), t
            }(),
            $ = function(t) {
                var i;
                return "object" != typeof t ? t : (e.each(t, function(a, n) {
                    /^[a-z]+_/.test(a) && (i = "", e.each(a.split("_"), function(e, t) {
                        i += e > 0 ? t.substr(0, 1).toUpperCase() + t.substr(1) : t
                    }), t[i] = n, delete t[a])
                }), t)
            },
            _ = function(t) {
                return e.inArray(t, x) > -1 ? i[t.toUpperCase()] : t
            },
            C = {
                youtube: {
                    reg: /https?:\/\/(?:[a-zA_Z]{2,3}.)?(?:youtube\.com\/watch\?)((?:[\w\d\-\_\=]+&amp;(?:amp;)?)*v(?:&lt;[A-Z]+&gt;)?=([0-9a-zA-Z\-\_]+))/i,
                    embed: function() {
                        return "http://www.youtube.com/embed/" + this.id
                    },
                    getUrl: function() {
                        return "https://www.googleapis.com/youtube/v3/videos?id=" + this.id + "&part=snippet&key=AIzaSyC08Y7Un4Zg4yjVXoYla14WJj5TzU2X-EQ&callback=?"
                    },
                    get_thumb: function(e) {
                        return p + "//img.youtube.com/vi/" + this.id + "/default.jpg"
                    },
                    get_image: function(e) {
                        return e.items[0].snippet.thumbnails.maxres ? p + "//img.youtube.com/vi/" + this.id + "/maxresdefault.jpg" : e.items[0].snippet.thumbnails.high ? p + "//img.youtube.com/vi/" + this.id + "/sddefault.jpg" : p + "//img.youtube.com/vi/" + this.id + "/hqdefault.jpg"
                    }
                },
                vimeo: {
                    reg: /https?:\/\/(?:www\.)?(vimeo\.com)\/(?:hd#)?([0-9]+)/i,
                    embed: function() {
                        return "http://player.vimeo.com/video/" + this.id
                    },
                    getUrl: function() {
                        return p + "//vimeo.com/api/v2/video/" + this.id + ".json?callback=?"
                    },
                    get_thumb: function(e) {
                        return e[0].thumbnail_medium
                    },
                    get_image: function(e) {
                        return e[0].thumbnail_large
                    }
                },
                dailymotion: {
                    reg: /https?:\/\/(?:www\.)?(dailymotion\.com)\/video\/([^_]+)/,
                    embed: function() {
                        return p + "//www.dailymotion.com/embed/video/" + this.id
                    },
                    getUrl: function() {
                        return "https://api.dailymotion.com/video/" + this.id + "?fields=thumbnail_240_url,thumbnail_720_url&callback=?"
                    },
                    get_thumb: function(e) {
                        return e.thumbnail_240_url
                    },
                    get_image: function(e) {
                        return e.thumbnail_720_url
                    }
                },
                _inst: []
            },
            k = function(t, i) {
                for (var a = 0; a < C._inst.length; a++)
                    if (C._inst[a].id === i && C._inst[a].type == t) return C._inst[a];
                this.type = t, this.id = i, this.readys = [], C._inst.push(this);
                var n = this;
                e.extend(this, C[t]), e.getJSON(this.getUrl(), function(t) {
                    n.data = t, e.each(n.readys, function(e, t) {
                        t(n.data)
                    }), n.readys = []
                }), this.getMedia = function(e, t, i) {
                    i = i || g;
                    var a = this,
                        n = function(i) {
                            t(a["get_" + e](i))
                        };
                    try {
                        a.data ? n(a.data) : a.readys.push(n)
                    } catch (e) {
                        i()
                    }
                }
            },
            T = function(e) {
                var t;
                for (var i in C)
                    if ((t = e && C[i].reg && e.match(C[i].reg)) && t.length) return {
                        id: t[2],
                        provider: i
                    };
                return !1
            },
            D = {
                support: function() {
                    var e = y().html;
                    return !w && (e.requestFullscreen || e.msRequestFullscreen || e.mozRequestFullScreen || e.webkitRequestFullScreen)
                }(),
                callback: g,
                enter: function(e, t, i) {
                    this.instance = e, this.callback = t || g, i = i || y().html, i.requestFullscreen ? i.requestFullscreen() : i.msRequestFullscreen ? i.msRequestFullscreen() : i.mozRequestFullScreen ? i.mozRequestFullScreen() : i.webkitRequestFullScreen && i.webkitRequestFullScreen()
                },
                exit: function(e) {
                    this.callback = e || g, n.exitFullscreen ? n.exitFullscreen() : n.msExitFullscreen ? n.msExitFullscreen() : n.mozCancelFullScreen ? n.mozCancelFullScreen() : n.webkitCancelFullScreen && n.webkitCancelFullScreen()
                },
                instance: null,
                listen: function() {
                    if (this.support) {
                        var e = function() {
                            if (D.instance) {
                                var e = D.instance._fullscreen;
                                n.fullscreen || n.mozFullScreen || n.webkitIsFullScreen || n.msFullscreenElement && null !== n.msFullscreenElement ? e._enter(D.callback) : e._exit(D.callback)
                            }
                        };
                        n.addEventListener("fullscreenchange", e, !1), n.addEventListener("MSFullscreenChange", e, !1), n.addEventListener("mozfullscreenchange", e, !1), n.addEventListener("webkitfullscreenchange", e, !1)
                    }
                }
            },
            I = [],
            S = [],
            A = !1,
            E = !1,
            L = [],
            M = [],
            O = function(t) {
                M.push(t), e.each(L, function(e, i) {
                    i._options.theme != t.name && (i._initialized || i._options.theme) || (i.theme = t, i._init.call(i))
                })
            },
            z = function() {
                return {
                    clearTimer: function(t) {
                        e.each(i.get(), function() {
                            this.clearTimer(t)
                        })
                    },
                    addTimer: function(t) {
                        e.each(i.get(), function() {
                            this.addTimer(t)
                        })
                    },
                    array: function(e) {
                        return s.slice.call(e, 0)
                    },
                    create: function(e, t) {
                        t = t || "div";
                        var i = n.createElement(t);
                        return i.className = e, i
                    },
                    removeFromArray: function(t, i) {
                        return e.each(t, function(e, a) {
                            if (a == i) return t.splice(e, 1), !1
                        }), t
                    },
                    getScriptPath: function(t) {
                        t = t || e("script:last").attr("src");
                        var i = t.split("/");
                        return 1 == i.length ? "" : (i.pop(), i.join("/") + "/")
                    },
                    animate: function() {
                        var a, o, r, s, l, c, h, d = function(e) {
                                var i, a = "transition WebkitTransition MozTransition OTransition".split(" ");
                                if (t.opera) return !1;
                                for (i = 0; a[i]; i++)
                                    if (void 0 !== e[a[i]]) return a[i];
                                return !1
                            }((n.body || n.documentElement).style),
                            u = {
                                MozTransition: "transitionend",
                                OTransition: "oTransitionEnd",
                                WebkitTransition: "webkitTransitionEnd",
                                transition: "transitionend"
                            } [d],
                            p = {
                                _default: [.25, .1, .25, 1],
                                galleria: [.645, .045, .355, 1],
                                galleriaIn: [.55, .085, .68, .53],
                                galleriaOut: [.25, .46, .45, .94],
                                ease: [.25, 0, .25, 1],
                                linear: [.25, .25, .75, .75],
                                "ease-in": [.42, 0, 1, 1],
                                "ease-out": [0, 0, .58, 1],
                                "ease-in-out": [.42, 0, .58, 1]
                            },
                            f = function(t, i, a) {
                                var n = {};
                                a = a || "transition", e.each("webkit moz ms o".split(" "), function() {
                                    n["-" + this + "-" + a] = i
                                }), t.css(n)
                            },
                            m = function(e) {
                                f(e, "none", "transition"), i.WEBKIT && i.TOUCH && (f(e, "translate3d(0,0,0)", "transform"), e.data("revert") && (e.css(e.data("revert")), e.data("revert", null)))
                            };
                        return function(n, v, y) {
                            return y = e.extend({
                                duration: 400,
                                complete: g,
                                stop: !1
                            }, y), n = e(n), y.duration ? d ? (y.stop && (n.off(u), m(n)), a = !1, e.each(v, function(e, t) {
                                h = n.css(e), z.parseValue(h) != z.parseValue(t) && (a = !0), n.css(e, h)
                            }), a ? (o = [], r = y.easing in p ? p[y.easing] : p._default, s = " " + y.duration + "ms cubic-bezier(" + r.join(",") + ")", void t.setTimeout(function(t, a, n, r) {
                                return function() {
                                    t.one(a, function(e) {
                                        return function() {
                                            m(e), y.complete.call(e[0])
                                        }
                                    }(t)), i.WEBKIT && i.TOUCH && (l = {}, c = [0, 0, 0], e.each(["left", "top"], function(e, i) {
                                        i in n && (c[e] = z.parseValue(n[i]) - z.parseValue(t.css(i)) + "px", l[i] = n[i], delete n[i])
                                    }), (c[0] || c[1]) && (t.data("revert", l), o.push("-webkit-transform" + r), f(t, "translate3d(" + c.join(",") + ")", "transform"))), e.each(n, function(e, t) {
                                        o.push(e + r)
                                    }), f(t, o.join(",")), t.css(n)
                                }
                            }(n, u, v, s), 2)) : void t.setTimeout(function() {
                                y.complete.call(n[0])
                            }, y.duration)) : void n.animate(v, y) : (n.css(v), void y.complete.call(n[0]))
                        }
                    }(),
                    removeAlpha: function(e) {
                        if (e instanceof jQuery && (e = e[0]), v < 9 && e) {
                            var t = e.style,
                                i = e.currentStyle,
                                a = i && i.filter || t.filter || "";
                            /alpha/.test(a) && (t.filter = a.replace(/alpha\([^)]*\)/i, ""))
                        }
                    },
                    forceStyles: function(t, i) {
                        t = e(t), t.attr("style") && t.data("styles", t.attr("style")).removeAttr("style"), t.css(i)
                    },
                    revertStyles: function() {
                        e.each(z.array(arguments), function(t, i) {
                            i = e(i), i.removeAttr("style"), i.attr("style", ""), i.data("styles") && i.attr("style", i.data("styles")).data("styles", null)
                        })
                    },
                    moveOut: function(e) {
                        z.forceStyles(e, {
                            position: "absolute",
                            left: -1e4
                        })
                    },
                    moveIn: function() {
                        z.revertStyles.apply(z, z.array(arguments))
                    },
                    hide: function(t, i, a) {
                        a = a || g;
                        var n = e(t);
                        t = n[0], n.data("opacity") || n.data("opacity", n.css("opacity"));
                        var o = {
                            opacity: 0
                        };
                        if (i) {
                            var r = v < 9 && t ? function() {
                                z.removeAlpha(t), t.style.visibility = "hidden", a.call(t)
                            } : a;
                            z.animate(t, o, {
                                duration: i,
                                complete: r,
                                stop: !0
                            })
                        } else v < 9 && t ? (z.removeAlpha(t), t.style.visibility = "hidden") : n.css(o)
                    },
                    show: function(t, i, a) {
                        a = a || g;
                        var n = e(t);
                        t = n[0];
                        var o = parseFloat(n.data("opacity")) || 1,
                            r = {
                                opacity: o
                            };
                        if (i) {
                            v < 9 && (n.css("opacity", 0), t.style.visibility = "visible");
                            var s = v < 9 && t ? function() {
                                1 == r.opacity && z.removeAlpha(t), a.call(t)
                            } : a;
                            z.animate(t, r, {
                                duration: i,
                                complete: s,
                                stop: !0
                            })
                        } else v < 9 && 1 == r.opacity && t ? (z.removeAlpha(t), t.style.visibility = "visible") : n.css(r)
                    },
                    wait: function(a) {
                        i._waiters = i._waiters || [], a = e.extend({
                            until: m,
                            success: g,
                            error: function() {
                                i.raise("Could not complete wait function.")
                            },
                            timeout: 3e3
                        }, a);
                        var n, o, r, s = z.timestamp(),
                            l = function() {
                                return o = z.timestamp(), n = o - s, z.removeFromArray(i._waiters, r), a.until(n) ? (a.success(), !1) : "number" == typeof a.timeout && o >= s + a.timeout ? (a.error(), !1) : void i._waiters.push(r = t.setTimeout(l, 10))
                            };
                        i._waiters.push(r = t.setTimeout(l, 10))
                    },
                    toggleQuality: function(e, t) {
                        7 !== v && 8 !== v || !e || "IMG" != e.nodeName.toUpperCase() || (void 0 === t && (t = "nearest-neighbor" === e.style.msInterpolationMode), e.style.msInterpolationMode = t ? "bicubic" : "nearest-neighbor")
                    },
                    insertStyleTag: function(t, i) {
                        if (!i || !e("#" + i).length) {
                            var a = n.createElement("style");
                            if (i && (a.id = i), y().head.appendChild(a), a.styleSheet) a.styleSheet.cssText = t;
                            else {
                                var o = n.createTextNode(t);
                                a.appendChild(o)
                            }
                        }
                    },
                    loadScript: function(t, i) {
                        var a = !1,
                            n = e("<script>").attr({
                                src: t,
                                async: !0
                            }).get(0);
                        n.onload = n.onreadystatechange = function() {
                            a || this.readyState && "loaded" !== this.readyState && "complete" !== this.readyState || (a = !0, n.onload = n.onreadystatechange = null, "function" == typeof i && i.call(this, this))
                        }, y().head.appendChild(n)
                    },
                    parseValue: function(e) {
                        if ("number" == typeof e) return e;
                        if ("string" == typeof e) {
                            var t = e.match(/\-?\d|\./g);
                            return t && t.constructor === Array ? 1 * t.join("") : 0
                        }
                        return 0
                    },
                    timestamp: function() {
                        return (new Date).getTime()
                    },
                    loadCSS: function(t, o, r) {
                        var s, l;
                        if (e("link[rel=stylesheet]").each(function() {
                                if (new RegExp(t).test(this.href)) return s = this, !1
                            }), "function" == typeof o && (r = o, o = a), r = r || g, s) return r.call(s, s), s;
                        if (l = n.styleSheets.length, e("#" + o).length) e("#" + o).attr("href", t), l--;
                        else {
                            s = e("<link>").attr({
                                rel: "stylesheet",
                                href: t,
                                id: o
                            }).get(0);
                            var c = e('link[rel="stylesheet"], style');
                            if (c.length ? c.get(0).parentNode.insertBefore(s, c[0]) : y().head.appendChild(s), v && l >= 31) return void i.raise("You have reached the browser stylesheet limit (31)", !0)
                        }
                        if ("function" == typeof r) {
                            var h = e("<s>").attr("id", "galleria-loader").hide().appendTo(y().body);
                            z.wait({
                                until: function() {
                                    return 1 == h.height()
                                },
                                success: function() {
                                    h.remove(), r.call(s, s)
                                },
                                error: function() {
                                    h.remove(), i.raise("Theme CSS could not load after 20 sec. " + (i.QUIRK ? "Your browser is in Quirks Mode, please add a correct doctype." : "Please download the latest theme at http://galleria.io/customer/."), !0)
                                },
                                timeout: 5e3
                            })
                        }
                        return s
                    }
                }
            }(),
            P = function(t) {
                return z.insertStyleTag(".galleria-videoicon{width:60px;height:60px;position:absolute;top:50%;left:50%;z-index:1;margin:-30px 0 0 -30px;cursor:pointer;background:#000;background:rgba(0,0,0,.8);border-radius:3px;-webkit-transition:all 150ms}.galleria-videoicon i{width:0px;height:0px;border-style:solid;border-width:10px 0 10px 16px;display:block;border-color:transparent transparent transparent #ffffff;margin:20px 0 0 22px}.galleria-image:hover .galleria-videoicon{background:#000}", "galleria-videoicon"), e(z.create("galleria-videoicon")).html("<i></i>").appendTo(t).click(function() {
                    e(this).siblings("img").mouseup()
                })
            },
            H = function() {
                var t = function(t, i, a, n) {
                    var o = this.getOptions("easing"),
                        r = this.getStageWidth(),
                        s = {
                            left: r * (t.rewind ? -1 : 1)
                        },
                        l = {
                            left: 0
                        };
                    a ? (s.opacity = 0, l.opacity = 1) : s.opacity = 1, e(t.next).css(s), z.animate(t.next, l, {
                        duration: t.speed,
                        complete: function(e) {
                            return function() {
                                i(), e.css({
                                    left: 0
                                })
                            }
                        }(e(t.next).add(t.prev)),
                        queue: !1,
                        easing: o
                    }), n && (t.rewind = !t.rewind), t.prev && (s = {
                        left: 0
                    }, l = {
                        left: r * (t.rewind ? 1 : -1)
                    }, a && (s.opacity = 1, l.opacity = 0), e(t.prev).css(s), z.animate(t.prev, l, {
                        duration: t.speed,
                        queue: !1,
                        easing: o,
                        complete: function() {
                            e(this).css("opacity", 0)
                        }
                    }))
                };
                return {
                    active: !1,
                    init: function(e, t, i) {
                        H.effects.hasOwnProperty(e) && H.effects[e].call(this, t, i)
                    },
                    effects: {
                        fade: function(t, i) {
                            e(t.next).css({
                                opacity: 0,
                                left: 0
                            }), z.animate(t.next, {
                                opacity: 1
                            }, {
                                duration: t.speed,
                                complete: i
                            }), t.prev && (e(t.prev).css("opacity", 1).show(), z.animate(t.prev, {
                                opacity: 0
                            }, {
                                duration: t.speed
                            }))
                        },
                        flash: function(t, i) {
                            e(t.next).css({
                                opacity: 0,
                                left: 0
                            }), t.prev ? z.animate(t.prev, {
                                opacity: 0
                            }, {
                                duration: t.speed / 2,
                                complete: function() {
                                    z.animate(t.next, {
                                        opacity: 1
                                    }, {
                                        duration: t.speed,
                                        complete: i
                                    })
                                }
                            }) : z.animate(t.next, {
                                opacity: 1
                            }, {
                                duration: t.speed,
                                complete: i
                            })
                        },
                        pulse: function(t, i) {
                            t.prev && e(t.prev).hide(), e(t.next).css({
                                opacity: 0,
                                left: 0
                            }).show(), z.animate(t.next, {
                                opacity: 1
                            }, {
                                duration: t.speed,
                                complete: i
                            })
                        },
                        slide: function(e, i) {
                            t.apply(this, z.array(arguments))
                        },
                        fadeslide: function(e, i) {
                            t.apply(this, z.array(arguments).concat([!0]))
                        },
                        doorslide: function(e, i) {
                            t.apply(this, z.array(arguments).concat([!1, !0]))
                        }
                    }
                }
            }();
        D.listen(), e.event.special["click:fast"] = {
                propagate: !0,
                add: function(i) {
                    var a = function(e) {
                            if (e.touches && e.touches.length) {
                                var t = e.touches[0];
                                return {
                                    x: t.pageX,
                                    y: t.pageY
                                }
                            }
                        },
                        n = {
                            touched: !1,
                            touchdown: !1,
                            coords: {
                                x: 0,
                                y: 0
                            },
                            evObj: {}
                        };
                    e(this).data({
                        clickstate: n,
                        timer: 0
                    }).on("touchstart.fast", function(i) {
                        t.clearTimeout(e(this).data("timer")), e(this).data("clickstate", {
                            touched: !0,
                            touchdown: !0,
                            coords: a(i.originalEvent),
                            evObj: i
                        })
                    }).on("touchmove.fast", function(t) {
                        var i = a(t.originalEvent),
                            n = e(this).data("clickstate");
                        Math.max(Math.abs(n.coords.x - i.x), Math.abs(n.coords.y - i.y)) > 6 && e(this).data("clickstate", e.extend(n, {
                            touchdown: !1
                        }))
                    }).on("touchend.fast", function(a) {
                        var o = e(this);
                        o.data("clickstate").touchdown && i.handler.call(this, a), o.data("timer", t.setTimeout(function() {
                            o.data("clickstate", n)
                        }, 400))
                    }).on("click.fast", function(t) {
                        if (e(this).data("clickstate").touched) return !1;
                        e(this).data("clickstate", n), i.handler.call(this, t)
                    })
                },
                remove: function() {
                    e(this).off("touchstart.fast touchmove.fast touchend.fast click.fast")
                }
            }, r.on("orientationchange", function() {
                e(this).resize()
            }), i = function() {
                var s = this;
                this._options = {}, this._playing = !1, this._playtime = 5e3, this._active = null, this._queue = {
                    length: 0
                }, this._data = [], this._dom = {}, this._thumbnails = [], this._layers = [], this._initialized = !1, this._firstrun = !1, this._stageWidth = 0, this._stageHeight = 0, this._target = a, this._binds = [], this._id = parseInt(1e4 * f.random(), 10), e.each("container stage images image-nav image-nav-left image-nav-right info info-text info-title info-description thumbnails thumbnails-list thumbnails-container thumb-nav-left thumb-nav-right loader counter tooltip".split(" "), function(e, t) {
                    s._dom[t] = z.create("galleria-" + t)
                }), e.each("current total".split(" "), function(e, t) {
                    s._dom[t] = z.create("galleria-" + t, "span")
                });
                var l = this._keyboard = {
                        keys: {
                            UP: 38,
                            DOWN: 40,
                            LEFT: 37,
                            RIGHT: 39,
                            RETURN: 13,
                            ESCAPE: 27,
                            BACKSPACE: 8,
                            SPACE: 32
                        },
                        map: {},
                        bound: !1,
                        press: function(e) {
                            var t = e.keyCode || e.which;
                            t in l.map && "function" == typeof l.map[t] && l.map[t].call(s, e)
                        },
                        attach: function(e) {
                            var t, i;
                            for (t in e) e.hasOwnProperty(t) && (i = t.toUpperCase(), i in l.keys ? l.map[l.keys[i]] = e[t] : l.map[i] = e[t]);
                            l.bound || (l.bound = !0, o.on("keydown", l.press))
                        },
                        detach: function() {
                            l.bound = !1, l.map = {}, o.off("keydown", l.press)
                        }
                    },
                    c = this._controls = {
                        0: a,
                        1: a,
                        active: 0,
                        swap: function() {
                            c.active = c.active ? 0 : 1
                        },
                        getActive: function() {
                            return s._options.swipe ? c.slides[s._active] : c[c.active]
                        },
                        getNext: function() {
                            return s._options.swipe ? c.slides[s.getNext(s._active)] : c[1 - c.active]
                        },
                        slides: [],
                        frames: [],
                        layers: []
                    },
                    h = this._carousel = {
                        next: s.$("thumb-nav-right"),
                        prev: s.$("thumb-nav-left"),
                        width: 0,
                        current: 0,
                        max: 0,
                        hooks: [],
                        update: function() {
                            var t = 0,
                                i = 0,
                                a = [0];
                            e.each(s._thumbnails, function(n, o) {
                                if (o.ready) {
                                    t += o.outerWidth || e(o.container).outerWidth(!0);
                                    var r = e(o.container).width();
                                    t += r - f.floor(r), a[n + 1] = t, i = f.max(i, o.outerHeight || e(o.container).outerHeight(!0))
                                }
                            }), s.$("thumbnails").css({
                                width: t,
                                height: i
                            }), h.max = t, h.hooks = a, h.width = s.$("thumbnails-list").width(), h.setClasses(), s.$("thumbnails-container").toggleClass("galleria-carousel", t > h.width), h.width = s.$("thumbnails-list").width()
                        },
                        bindControls: function() {
                            var e;
                            h.next.on("click:fast", function(t) {
                                if (t.preventDefault(), "auto" === s._options.carouselSteps) {
                                    for (e = h.current; e < h.hooks.length; e++)
                                        if (h.hooks[e] - h.hooks[h.current] > h.width) {
                                            h.set(e - 2);
                                            break
                                        }
                                } else h.set(h.current + s._options.carouselSteps)
                            }), h.prev.on("click:fast", function(t) {
                                if (t.preventDefault(), "auto" === s._options.carouselSteps)
                                    for (e = h.current; e >= 0; e--) {
                                        if (h.hooks[h.current] - h.hooks[e] > h.width) {
                                            h.set(e + 2);
                                            break
                                        }
                                        if (0 === e) {
                                            h.set(0);
                                            break
                                        }
                                    } else h.set(h.current - s._options.carouselSteps)
                            })
                        },
                        set: function(e) {
                            for (e = f.max(e, 0); h.hooks[e - 1] + h.width >= h.max && e >= 0;) e--;
                            h.current = e, h.animate()
                        },
                        getLast: function(e) {
                            return (e || h.current) - 1
                        },
                        follow: function(e) {
                            if (0 === e || e === h.hooks.length - 2) return void h.set(e);
                            for (var t = h.current; h.hooks[t] - h.hooks[h.current] < h.width && t <= h.hooks.length;) t++;
                            e - 1 < h.current ? h.set(e - 1) : e + 2 > t && h.set(e - t + h.current + 2)
                        },
                        setClasses: function() {
                            h.prev.toggleClass("disabled", !h.current), h.next.toggleClass("disabled", h.hooks[h.current] + h.width >= h.max)
                        },
                        animate: function(t) {
                            h.setClasses();
                            var i = -1 * h.hooks[h.current];
                            isNaN(i) || (s.$("thumbnails").css("left", function() {
                                return e(this).css("left")
                            }), z.animate(s.get("thumbnails"), {
                                left: i
                            }, {
                                duration: s._options.carouselSpeed,
                                easing: s._options.easing,
                                queue: !1
                            }))
                        }
                    },
                    u = this._tooltip = {
                        initialized: !1,
                        open: !1,
                        timer: "tooltip" + s._id,
                        swapTimer: "swap" + s._id,
                        init: function() {
                            u.initialized = !0, z.insertStyleTag(".galleria-tooltip{padding:3px 8px;max-width:50%;background:#ffe;color:#000;z-index:3;position:absolute;font-size:11px;line-height:1.3;opacity:0;box-shadow:0 0 2px rgba(0,0,0,.4);-moz-box-shadow:0 0 2px rgba(0,0,0,.4);-webkit-box-shadow:0 0 2px rgba(0,0,0,.4);}", "galleria-tooltip"), s.$("tooltip").css({
                                opacity: .8,
                                visibility: "visible",
                                display: "none"
                            })
                        },
                        move: function(e) {
                            var t = s.getMousePosition(e).x,
                                i = s.getMousePosition(e).y,
                                a = s.$("tooltip"),
                                n = t,
                                o = i,
                                r = a.outerHeight(!0) + 1,
                                l = a.outerWidth(!0),
                                c = r + 15,
                                h = s.$("container").width() - l - 2,
                                d = s.$("container").height() - r - 2;
                            isNaN(n) || isNaN(o) || (n += 10, o -= r + 8, n = f.max(0, f.min(h, n)), o = f.max(0, f.min(d, o)), i < c && (o = c), a.css({
                                left: n,
                                top: o
                            }))
                        },
                        bind: function(t, a) {
                            if (!i.TOUCH) {
                                u.initialized || u.init();
                                var n = function() {
                                        s.$("container").off("mousemove", u.move), s.clearTimer(u.timer), s.$("tooltip").stop().animate({
                                            opacity: 0
                                        }, 200, function() {
                                            s.$("tooltip").hide(), s.addTimer(u.swapTimer, function() {
                                                u.open = !1
                                            }, 1e3)
                                        })
                                    },
                                    o = function(t, i) {
                                        u.define(t, i), e(t).hover(function() {
                                            s.clearTimer(u.swapTimer), s.$("container").off("mousemove", u.move).on("mousemove", u.move).trigger("mousemove"), u.show(t), s.addTimer(u.timer, function() {
                                                s.$("tooltip").stop().show().animate({
                                                    opacity: 1
                                                }), u.open = !0
                                            }, u.open ? 0 : 500)
                                        }, n).click(n)
                                    };
                                "string" == typeof a ? o(t in s._dom ? s.get(t) : t, a) : e.each(t, function(e, t) {
                                    o(s.get(e), t)
                                })
                            }
                        },
                        show: function(i) {
                            i = e(i in s._dom ? s.get(i) : i);
                            var a = i.data("tt"),
                                n = function(e) {
                                    t.setTimeout(function(e) {
                                        return function() {
                                            u.move(e)
                                        }
                                    }(e), 10), i.off("mouseup", n)
                                };
                            (a = "function" == typeof a ? a() : a) && (s.$("tooltip").html(a.replace(/\s/, "&#160;")), i.on("mouseup", n))
                        },
                        define: function(t, i) {
                            if ("function" != typeof i) {
                                var a = i;
                                i = function() {
                                    return a
                                }
                            }
                            t = e(t in s._dom ? s.get(t) : t).data("tt", i), u.show(t)
                        }
                    },
                    p = this._fullscreen = {
                        scrolled: 0,
                        crop: a,
                        active: !1,
                        prev: e(),
                        beforeEnter: function(e) {
                            e()
                        },
                        beforeExit: function(e) {
                            e()
                        },
                        keymap: s._keyboard.map,
                        parseCallback: function(t, i) {
                            return H.active ? function() {
                                "function" == typeof t && t.call(s);
                                var a = s._controls.getActive(),
                                    n = s._controls.getNext();
                                s._scaleImage(n), s._scaleImage(a), i && s._options.trueFullscreen && e(a.container).add(n.container).trigger("transitionend")
                            } : t
                        },
                        enter: function(e) {
                            p.beforeEnter(function() {
                                e = p.parseCallback(e, !0), s._options.trueFullscreen && D.support ? (p.active = !0, z.forceStyles(s.get("container"), {
                                    width: "100%",
                                    height: "100%"
                                }), s.rescale(), i.MAC ? i.SAFARI && /version\/[1-5]/.test(d) ? (s.$("stage").css("opacity", 0), t.setTimeout(function() {
                                    p.scale(), s.$("stage").css("opacity", 1)
                                }, 4)) : (s.$("container").css("opacity", 0).addClass("fullscreen"), t.setTimeout(function() {
                                    p.scale(), s.$("container").css("opacity", 1)
                                }, 50)) : s.$("container").addClass("fullscreen"), r.resize(p.scale), D.enter(s, e, s.get("container"))) : (p.scrolled = r.scrollTop(), i.TOUCH || t.scrollTo(0, 0), p._enter(e))
                            })
                        },
                        _enter: function(o) {
                            p.active = !0, w && (p.iframe = function() {
                                var a, o = n.referrer,
                                    r = n.createElement("a"),
                                    s = t.location;
                                return r.href = o, r.protocol != s.protocol || r.hostname != s.hostname || r.port != s.port ? (i.raise("Parent fullscreen not available. Iframe protocol, domains and ports must match."), !1) : (p.pd = t.parent.document, e(p.pd).find("iframe").each(function() {
                                    if ((this.contentDocument || this.contentWindow.document) === n) return a = this, !1
                                }), a)
                            }()), z.hide(s.getActiveImage()), w && p.iframe && (p.iframe.scrolled = e(t.parent).scrollTop(), t.parent.scrollTo(0, 0));
                            var l = s.getData(),
                                c = s._options,
                                h = !s._options.trueFullscreen || !D.support,
                                d = {
                                    height: "100%",
                                    overflow: "hidden",
                                    margin: 0,
                                    padding: 0
                                };
                            if (h && (s.$("container").addClass("fullscreen"), p.prev = s.$("container").prev(), p.prev.length || (p.parent = s.$("container").parent()), s.$("container").appendTo("body"),
                                    z.forceStyles(s.get("container"), {
                                        position: i.TOUCH ? "absolute" : "fixed",
                                        top: 0,
                                        left: 0,
                                        width: "100%",
                                        height: "100%",
                                        zIndex: 1e4
                                    }), z.forceStyles(y().html, d), z.forceStyles(y().body, d)), w && p.iframe && (z.forceStyles(p.pd.documentElement, d), z.forceStyles(p.pd.body, d), z.forceStyles(p.iframe, e.extend(d, {
                                    width: "100%",
                                    height: "100%",
                                    top: 0,
                                    left: 0,
                                    position: "fixed",
                                    zIndex: 1e4,
                                    border: "none"
                                }))), p.keymap = e.extend({}, s._keyboard.map), s.attachKeyboard({
                                    escape: s.exitFullscreen,
                                    right: s.next,
                                    left: s.prev
                                }), p.crop = c.imageCrop, c.fullscreenCrop != a && (c.imageCrop = c.fullscreenCrop), l && l.big && l.image !== l.big) {
                                var u = new i.Picture,
                                    f = u.isCached(l.big),
                                    g = s.getIndex(),
                                    m = s._thumbnails[g];
                                s.trigger({
                                    type: i.LOADSTART,
                                    cached: f,
                                    rewind: !1,
                                    index: g,
                                    imageTarget: s.getActiveImage(),
                                    thumbTarget: m,
                                    galleriaData: l
                                }), u.load(l.big, function(t) {
                                    s._scaleImage(t, {
                                        complete: function(t) {
                                            s.trigger({
                                                type: i.LOADFINISH,
                                                cached: f,
                                                index: g,
                                                rewind: !1,
                                                imageTarget: t.image,
                                                thumbTarget: m
                                            });
                                            var a = s._controls.getActive().image;
                                            a && e(a).width(t.image.width).height(t.image.height).attr("style", e(t.image).attr("style")).attr("src", t.image.src)
                                        }
                                    })
                                });
                                var v = s.getNext(g),
                                    b = new i.Picture,
                                    x = s.getData(v);
                                b.preload(s.isFullscreen() && x.big ? x.big : x.image)
                            }
                            s.rescale(function() {
                                s.addTimer(!1, function() {
                                    h && z.show(s.getActiveImage()), "function" == typeof o && o.call(s), s.rescale()
                                }, 100), s.trigger(i.FULLSCREEN_ENTER)
                            }), h ? r.resize(p.scale) : z.show(s.getActiveImage())
                        },
                        scale: function() {
                            s.rescale()
                        },
                        exit: function(e) {
                            p.beforeExit(function() {
                                e = p.parseCallback(e), s._options.trueFullscreen && D.support ? D.exit(e) : p._exit(e)
                            })
                        },
                        _exit: function(e) {
                            p.active = !1;
                            var a = !s._options.trueFullscreen || !D.support,
                                n = s.$("container").removeClass("fullscreen");
                            if (p.parent ? p.parent.prepend(n) : n.insertAfter(p.prev), a) {
                                z.hide(s.getActiveImage()), z.revertStyles(s.get("container"), y().html, y().body), i.TOUCH || t.scrollTo(0, p.scrolled);
                                var o = s._controls.frames[s._controls.active];
                                o && o.image && (o.image.src = o.image.src)
                            }
                            w && p.iframe && (z.revertStyles(p.pd.documentElement, p.pd.body, p.iframe), p.iframe.scrolled && t.parent.scrollTo(0, p.iframe.scrolled)), s.detachKeyboard(), s.attachKeyboard(p.keymap), s._options.imageCrop = p.crop;
                            var l = s.getData().big,
                                c = s._controls.getActive().image;
                            !s.getData().iframe && c && l && l == c.src && t.setTimeout(function(e) {
                                return function() {
                                    c.src = e
                                }
                            }(s.getData().image), 1), s.rescale(function() {
                                s.addTimer(!1, function() {
                                    a && z.show(s.getActiveImage()), "function" == typeof e && e.call(s), r.trigger("resize")
                                }, 50), s.trigger(i.FULLSCREEN_EXIT)
                            }), r.off("resize", p.scale)
                        }
                    },
                    g = this._idle = {
                        trunk: [],
                        bound: !1,
                        active: !1,
                        add: function(t, a, n, o) {
                            if (t && !i.TOUCH) {
                                g.bound || g.addEvent(), t = e(t), "boolean" == typeof n && (o = n, n = {}), n = n || {};
                                var r, s = {};
                                for (r in a) a.hasOwnProperty(r) && (s[r] = t.css(r));
                                t.data("idle", {
                                    from: e.extend(s, n),
                                    to: a,
                                    complete: !0,
                                    busy: !1
                                }), o ? t.css(a) : g.addTimer(), g.trunk.push(t)
                            }
                        },
                        remove: function(t) {
                            t = e(t), e.each(g.trunk, function(e, i) {
                                i && i.length && !i.not(t).length && (t.css(t.data("idle").from), g.trunk.splice(e, 1))
                            }), g.trunk.length || (g.removeEvent(), s.clearTimer(g.timer))
                        },
                        addEvent: function() {
                            g.bound = !0, s.$("container").on("mousemove click", g.showAll), "hover" == s._options.idleMode && s.$("container").on("mouseleave", g.hide)
                        },
                        removeEvent: function() {
                            g.bound = !1, s.$("container").on("mousemove click", g.showAll), "hover" == s._options.idleMode && s.$("container").off("mouseleave", g.hide)
                        },
                        addTimer: function() {
                            "hover" != s._options.idleMode && s.addTimer("idle", function() {
                                g.hide()
                            }, s._options.idleTime)
                        },
                        hide: function() {
                            if (s._options.idleMode && !1 !== s.getIndex()) {
                                s.trigger(i.IDLE_ENTER);
                                var t = g.trunk.length;
                                e.each(g.trunk, function(e, i) {
                                    var a = i.data("idle");
                                    a && (i.data("idle").complete = !1, z.animate(i, a.to, {
                                        duration: s._options.idleSpeed,
                                        complete: function() {
                                            e == t - 1 && (g.active = !1)
                                        }
                                    }))
                                })
                            }
                        },
                        showAll: function() {
                            s.clearTimer("idle"), e.each(g.trunk, function(e, t) {
                                g.show(t)
                            })
                        },
                        show: function(t) {
                            var a = t.data("idle");
                            g.active && (a.busy || a.complete) || (a.busy = !0, s.trigger(i.IDLE_EXIT), s.clearTimer("idle"), z.animate(t, a.from, {
                                duration: s._options.idleSpeed / 2,
                                complete: function() {
                                    g.active = !0, e(t).data("idle").busy = !1, e(t).data("idle").complete = !0
                                }
                            })), g.addTimer()
                        }
                    },
                    m = this._lightbox = {
                        width: 0,
                        height: 0,
                        initialized: !1,
                        active: null,
                        image: null,
                        elems: {},
                        keymap: !1,
                        init: function() {
                            if (!m.initialized) {
                                m.initialized = !0;
                                var t = {},
                                    a = s._options,
                                    n = "",
                                    o = "position:absolute;",
                                    r = "lightbox-",
                                    l = {
                                        overlay: "position:fixed;display:none;opacity:" + a.overlayOpacity + ";filter:alpha(opacity=" + 100 * a.overlayOpacity + ");top:0;left:0;width:100%;height:100%;background:" + a.overlayBackground + ";z-index:99990",
                                        box: "position:fixed;display:none;width:400px;height:400px;top:50%;left:50%;margin-top:-200px;margin-left:-200px;z-index:99991",
                                        shadow: o + "background:#000;width:100%;height:100%;",
                                        content: o + "background-color:#fff;top:10px;left:10px;right:10px;bottom:10px;overflow:hidden",
                                        info: o + "bottom:10px;left:10px;right:10px;color:#444;font:11px/13px arial,sans-serif;height:13px",
                                        close: o + "top:10px;right:10px;height:20px;width:20px;background:#fff;text-align:center;cursor:pointer;color:#444;font:16px/22px arial,sans-serif;z-index:99999",
                                        image: o + "top:10px;left:10px;right:10px;bottom:30px;overflow:hidden;display:block;",
                                        prevholder: o + "width:50%;top:0;bottom:40px;cursor:pointer;",
                                        nextholder: o + "width:50%;top:0;bottom:40px;right:-1px;cursor:pointer;",
                                        prev: o + "top:50%;margin-top:-20px;height:40px;width:30px;background:#fff;left:20px;display:none;text-align:center;color:#000;font:bold 16px/36px arial,sans-serif",
                                        next: o + "top:50%;margin-top:-20px;height:40px;width:30px;background:#fff;right:20px;left:auto;display:none;font:bold 16px/36px arial,sans-serif;text-align:center;color:#000",
                                        title: "float:left",
                                        counter: "float:right;margin-left:8px;"
                                    },
                                    c = {},
                                    h = "";
                                h = v > 7 ? v < 9 ? "background:#000;filter:alpha(opacity=0);" : "background:rgba(0,0,0,0);" : "z-index:99999", l.nextholder += h, l.prevholder += h, e.each(l, function(e, t) {
                                        n += ".galleria-" + r + e + "{" + t + "}"
                                    }), n += ".galleria-lightbox-box.iframe .galleria-lightbox-prevholder,.galleria-lightbox-box.iframe .galleria-lightbox-nextholder{width:100px;height:100px;top:50%;margin-top:-70px}", z.insertStyleTag(n, "galleria-lightbox"), e.each("overlay box content shadow title info close prevholder prev nextholder next counter image".split(" "), function(e, i) {
                                        s.addElement("lightbox-" + i), t[i] = m.elems[i] = s.get("lightbox-" + i)
                                    }), m.image = new i.Picture, e.each({
                                        box: "shadow content close prevholder nextholder",
                                        info: "title counter",
                                        content: "info image",
                                        prevholder: "prev",
                                        nextholder: "next"
                                    }, function(t, i) {
                                        var a = [];
                                        e.each(i.split(" "), function(e, t) {
                                            a.push(r + t)
                                        }), c[r + t] = a
                                    }), s.append(c), e(t.image).append(m.image.container), e(y().body).append(t.overlay, t.box),
                                    function(t) {
                                        t.hover(function() {
                                            e(this).css("color", "#bbb")
                                        }, function() {
                                            e(this).css("color", "#444")
                                        })
                                    }(e(t.close).on("click:fast", m.hide).html("&#215;")), e.each(["Prev", "Next"], function(a, n) {
                                        var o = e(t[n.toLowerCase()]).html(/v/.test(n) ? "&#8249;&#160;" : "&#160;&#8250;"),
                                            r = e(t[n.toLowerCase() + "holder"]);
                                        if (r.on("click:fast", function() {
                                                m["show" + n]()
                                            }), v < 8 || i.TOUCH) return void o.show();
                                        r.hover(function() {
                                            o.show()
                                        }, function(e) {
                                            o.stop().fadeOut(200)
                                        })
                                    }), e(t.overlay).on("click:fast", m.hide), i.IPAD && (s._options.lightboxTransitionSpeed = 0)
                            }
                        },
                        rescale: function(t) {
                            var a = f.min(r.width() - 40, m.width),
                                n = f.min(r.height() - 60, m.height),
                                o = f.min(a / m.width, n / m.height),
                                l = f.round(m.width * o) + 40,
                                c = f.round(m.height * o) + 60,
                                h = {
                                    width: l,
                                    height: c,
                                    "margin-top": -1 * f.ceil(c / 2),
                                    "margin-left": -1 * f.ceil(l / 2)
                                };
                            t ? e(m.elems.box).css(h) : e(m.elems.box).animate(h, {
                                duration: s._options.lightboxTransitionSpeed,
                                easing: s._options.easing,
                                complete: function() {
                                    var t = m.image,
                                        a = s._options.lightboxFadeSpeed;
                                    s.trigger({
                                        type: i.LIGHTBOX_IMAGE,
                                        imageTarget: t.image
                                    }), e(t.container).show(), e(t.image).animate({
                                        opacity: 1
                                    }, a), z.show(m.elems.info, a)
                                }
                            })
                        },
                        hide: function() {
                            m.image.image = null, r.off("resize", m.rescale), e(m.elems.box).hide().find("iframe").remove(), z.hide(m.elems.info), s.detachKeyboard(), s.attachKeyboard(m.keymap), m.keymap = !1, z.hide(m.elems.overlay, 200, function() {
                                e(this).hide().css("opacity", s._options.overlayOpacity), s.trigger(i.LIGHTBOX_CLOSE)
                            })
                        },
                        showNext: function() {
                            m.show(s.getNext(m.active))
                        },
                        showPrev: function() {
                            m.show(s.getPrev(m.active))
                        },
                        show: function(a) {
                            m.active = a = "number" == typeof a ? a : s.getIndex() || 0, m.initialized || m.init(), s.trigger(i.LIGHTBOX_OPEN), m.keymap || (m.keymap = e.extend({}, s._keyboard.map), s.attachKeyboard({
                                escape: m.hide,
                                right: m.showNext,
                                left: m.showPrev
                            })), r.off("resize", m.rescale);
                            var n, o, l, c = s.getData(a),
                                h = s.getDataLength(),
                                d = s.getNext(a);
                            z.hide(m.elems.info);
                            try {
                                for (l = s._options.preload; l > 0; l--) o = new i.Picture, n = s.getData(d), o.preload(n.big ? n.big : n.image), d = s.getNext(d)
                            } catch (e) {}
                            m.image.isIframe = c.iframe && !c.image, e(m.elems.box).toggleClass("iframe", m.image.isIframe), e(m.image.container).find(".galleria-videoicon").remove(), m.image.load(c.big || c.image || c.iframe, function(i) {
                                if (i.isIframe) {
                                    var n = e(t).width(),
                                        o = e(t).height();
                                    if (i.video && s._options.maxVideoSize) {
                                        var l = f.min(s._options.maxVideoSize / n, s._options.maxVideoSize / o);
                                        l < 1 && (n *= l, o *= l)
                                    }
                                    m.width = n, m.height = o
                                } else m.width = i.original.width, m.height = i.original.height;
                                if (e(i.image).css({
                                        width: i.isIframe ? "100%" : "100.1%",
                                        height: i.isIframe ? "100%" : "100.1%",
                                        top: 0,
                                        bottom: 0,
                                        zIndex: 99998,
                                        opacity: 0,
                                        visibility: "visible"
                                    }).parent().height("100%"), m.elems.title.innerHTML = c.title || "", m.elems.counter.innerHTML = a + 1 + " / " + h, r.resize(m.rescale), m.rescale(), c.image && c.iframe) {
                                    if (e(m.elems.box).addClass("iframe"), c.video) {
                                        var d = P(i.container).hide();
                                        t.setTimeout(function() {
                                            d.fadeIn(200)
                                        }, 200)
                                    }
                                    e(i.image).css("cursor", "pointer").mouseup(function(t, i) {
                                        return function(a) {
                                            e(m.image.container).find(".galleria-videoicon").remove(), a.preventDefault(), i.isIframe = !0, i.load(t.iframe + (t.video ? "&autoplay=1" : ""), {
                                                width: "100%",
                                                height: v < 8 ? e(m.image.container).height() : "100%"
                                            })
                                        }
                                    }(c, i))
                                }
                            }), e(m.elems.overlay).show().css("visibility", "visible"), e(m.elems.box).show()
                        }
                    },
                    b = this._timer = {
                        trunk: {},
                        add: function(e, i, a, n) {
                            if (e = e || (new Date).getTime(), n = n || !1, this.clear(e), n) {
                                var o = i;
                                i = function() {
                                    o(), b.add(e, i, a)
                                }
                            }
                            this.trunk[e] = t.setTimeout(i, a)
                        },
                        clear: function(e) {
                            var i, a = function(e) {
                                t.clearTimeout(this.trunk[e]), delete this.trunk[e]
                            };
                            if (e && e in this.trunk) a.call(this, e);
                            else if (void 0 === e)
                                for (i in this.trunk) this.trunk.hasOwnProperty(i) && a.call(this, i)
                        }
                    };
                return this
            }, i.prototype = {
                constructor: i,
                init: function(t, n) {
                    if (n = $(n), this._original = {
                            target: t,
                            options: n,
                            data: null
                        }, this._target = this._dom.target = t.nodeName ? t : e(t).get(0), this._original.html = this._target.innerHTML, S.push(this), !this._target) return void i.raise("Target not found", !0);
                    if (this._options = {
                            autoplay: !1,
                            carousel: !0,
                            carouselFollow: !0,
                            carouselSpeed: 400,
                            carouselSteps: "auto",
                            clicknext: !1,
                            dailymotion: {
                                foreground: "%23EEEEEE",
                                highlight: "%235BCEC5",
                                background: "%23222222",
                                logo: 0,
                                hideInfos: 1
                            },
                            dataConfig: function(e) {
                                return {}
                            },
                            dataSelector: "img",
                            dataSort: !1,
                            dataSource: this._target,
                            debug: a,
                            dummy: a,
                            easing: "galleria",
                            extend: function(e) {},
                            fullscreenCrop: a,
                            fullscreenDoubleTap: !0,
                            fullscreenTransition: a,
                            height: 0,
                            idleMode: !0,
                            idleTime: 3e3,
                            idleSpeed: 200,
                            imageCrop: !1,
                            imageMargin: 0,
                            imagePan: !1,
                            imagePanSmoothness: 12,
                            imagePosition: "50%",
                            imageTimeout: a,
                            initialTransition: a,
                            keepSource: !1,
                            layerFollow: !0,
                            lightbox: !1,
                            lightboxFadeSpeed: 200,
                            lightboxTransitionSpeed: 200,
                            linkSourceImages: !0,
                            maxScaleRatio: a,
                            maxVideoSize: a,
                            minScaleRatio: a,
                            overlayOpacity: .85,
                            overlayBackground: "#0b0b0b",
                            pauseOnInteraction: !0,
                            popupLinks: !1,
                            preload: 2,
                            queue: !0,
                            responsive: !0,
                            show: 0,
                            showInfo: !0,
                            showCounter: !0,
                            showImagenav: !0,
                            swipe: "auto",
                            theme: null,
                            thumbCrop: !0,
                            thumbEventType: "click:fast",
                            thumbMargin: 0,
                            thumbQuality: "auto",
                            thumbDisplayOrder: !0,
                            thumbPosition: "50%",
                            thumbnails: !0,
                            touchTransition: a,
                            transition: "fade",
                            transitionInitial: a,
                            transitionSpeed: 400,
                            trueFullscreen: !0,
                            useCanvas: !1,
                            variation: "",
                            videoPoster: !0,
                            vimeo: {
                                title: 0,
                                byline: 0,
                                portrait: 0,
                                color: "aaaaaa"
                            },
                            wait: 5e3,
                            width: "auto",
                            youtube: {
                                modestbranding: 1,
                                autohide: 1,
                                color: "white",
                                hd: 1,
                                rel: 0,
                                showinfo: 0
                            }
                        }, this._options.initialTransition = this._options.initialTransition || this._options.transitionInitial, n && (!1 === n.debug && (l = !1), "number" == typeof n.imageTimeout && (c = n.imageTimeout), "string" == typeof n.dummy && (h = n.dummy), "string" == typeof n.theme && (this._options.theme = n.theme)), e(this._target).children().hide(), i.QUIRK && i.raise("Your page is in Quirks mode, Galleria may not render correctly. Please validate your HTML and add a correct doctype."), M.length)
                        if (this._options.theme) {
                            for (var o = 0; o < M.length; o++)
                                if (this._options.theme === M[o].name) {
                                    this.theme = M[o];
                                    break
                                }
                        } else this.theme = M[0];
                    return "object" == typeof this.theme ? this._init() : L.push(this), this
                },
                _init: function() {
                    var o = this,
                        s = this._options;
                    if (this._initialized) return i.raise("Init failed: Gallery instance already initialized."), this;
                    if (this._initialized = !0, !this.theme) return i.raise("Init failed: No theme found.", !0), this;
                    if (e.extend(!0, s, this.theme.defaults, this._original.options, i.configure.options), s.swipe = function(e) {
                            return "enforced" == e || !1 !== e && "disabled" != e && !!i.TOUCH
                        }(s.swipe), s.swipe && (s.clicknext = !1, s.imagePan = !1), function(e) {
                            if (!("getContext" in e)) return void(e = null);
                            E = E || {
                                elem: e,
                                context: e.getContext("2d"),
                                cache: {},
                                length: 0
                            }
                        }(n.createElement("canvas")), this.bind(i.DATA, function() {
                            t.screen && t.screen.width && Array.prototype.forEach && this._data.forEach(function(e) {
                                var i = "devicePixelRatio" in t ? t.devicePixelRatio : 1;
                                f.max(t.screen.width, t.screen.height) * i < 1024 && (e.big = e.image)
                            }), this._original.data = this._data, this.get("total").innerHTML = this.getDataLength();
                            var e = this.$("container");
                            o._options.height < 2 && (o._userRatio = o._ratio = o._options.height);
                            var a = {
                                    width: 0,
                                    height: 0
                                },
                                n = function() {
                                    return o.$("stage").height()
                                };
                            z.wait({
                                until: function() {
                                    return a = o._getWH(), e.width(a.width).height(a.height), n() && a.width && a.height > 50
                                },
                                success: function() {
                                    o._width = a.width, o._height = a.height, o._ratio = o._ratio || a.height / a.width, i.WEBKIT ? t.setTimeout(function() {
                                        o._run()
                                    }, 1) : o._run()
                                },
                                error: function() {
                                    n() ? i.raise("Could not extract sufficient width/height of the gallery container. Traced measures: width:" + a.width + "px, height: " + a.height + "px.", !0) : i.raise("Could not extract a stage height from the CSS. Traced height: " + n() + "px.", !0)
                                },
                                timeout: "number" == typeof this._options.wait && this._options.wait
                            })
                        }), this.append({
                            "info-text": ["info-title", "info-description"],
                            info: ["info-text"],
                            "image-nav": ["image-nav-right", "image-nav-left"],
                            stage: ["images", "loader", "counter", "image-nav"],
                            "thumbnails-list": ["thumbnails"],
                            "thumbnails-container": ["thumb-nav-left", "thumbnails-list", "thumb-nav-right"],
                            container: ["stage", "thumbnails-container", "info", "tooltip"]
                        }), z.hide(this.$("counter").append(this.get("current"), n.createTextNode(" / "), this.get("total"))), this.setCounter("&#8211;"), z.hide(o.get("tooltip")), this.$("container").addClass([i.TOUCH ? "touch" : "notouch", this._options.variation, "galleria-theme-" + this.theme.name].join(" ")), this._options.swipe || e.each(new Array(2), function(t) {
                            var a = new i.Picture;
                            e(a.container).css({
                                position: "absolute",
                                top: 0,
                                left: 0
                            }).prepend(o._layers[t] = e(z.create("galleria-layer")).css({
                                position: "absolute",
                                top: 0,
                                left: 0,
                                right: 0,
                                bottom: 0,
                                zIndex: 2
                            })[0]), o.$("images").append(a.container), o._controls[t] = a;
                            var n = new i.Picture;
                            n.isIframe = !0, e(n.container).attr("class", "galleria-frame").css({
                                position: "absolute",
                                top: 0,
                                left: 0,
                                zIndex: 4,
                                background: "#000",
                                display: "none"
                            }).appendTo(a.container), o._controls.frames[t] = n
                        }), this.$("images").css({
                            position: "relative",
                            top: 0,
                            left: 0,
                            width: "100%",
                            height: "100%"
                        }), s.swipe && (this.$("images").css({
                            position: "absolute",
                            top: 0,
                            left: 0,
                            width: 0,
                            height: "100%"
                        }), this.finger = new i.Finger(this.get("stage"), {
                            onchange: function(e) {
                                o.pause().show(e)
                            },
                            oncomplete: function(t) {
                                var i = f.max(0, f.min(parseInt(t, 10), o.getDataLength() - 1)),
                                    a = o.getData(i);
                                e(o._thumbnails[i].container).addClass("active").siblings(".active").removeClass("active"), a && (o.$("images").find(".galleria-frame").css("opacity", 0).hide().find("iframe").remove(), o._options.carousel && o._options.carouselFollow && o._carousel.follow(i))
                            }
                        }), this.bind(i.RESCALE, function() {
                            this.finger.setup()
                        }), this.$("stage").on("click", function(i) {
                            var n = o.getData();
                            if (n) {
                                if (n.iframe) {
                                    o.isPlaying() && o.pause();
                                    var r = o._controls.frames[o._active],
                                        s = o._stageWidth,
                                        l = o._stageHeight;
                                    if (e(r.container).find("iframe").length) return;
                                    return e(r.container).css({
                                        width: s,
                                        height: l,
                                        opacity: 0
                                    }).show().animate({
                                        opacity: 1
                                    }, 200), void t.setTimeout(function() {
                                        r.load(n.iframe + (n.video ? "&autoplay=1" : ""), {
                                            width: s,
                                            height: l
                                        }, function(e) {
                                            o.$("container").addClass("videoplay"), e.scale({
                                                width: o._stageWidth,
                                                height: o._stageHeight,
                                                iframelimit: n.video ? o._options.maxVideoSize : a
                                            })
                                        })
                                    }, 100)
                                }
                                n.link && (o._options.popupLinks ? t.open(n.link, "_blank") : t.location.href = n.link)
                            }
                        }), this.bind(i.IMAGE, function(t) {
                            o.setCounter(t.index), o.setInfo(t.index);
                            var i = this.getNext(),
                                a = this.getPrev(),
                                n = [a, i];
                            n.push(this.getNext(i), this.getPrev(a), o._controls.slides.length - 1);
                            var r = [];
                            e.each(n, function(t, i) {
                                -1 == e.inArray(i, r) && r.push(i)
                            }), e.each(r, function(t, i) {
                                var a = o.getData(i),
                                    n = o._controls.slides[i],
                                    r = o.isFullscreen() && a.big ? a.big : a.image || a.iframe;
                                a.iframe && !a.image && (n.isIframe = !0), n.ready || o._controls.slides[i].load(r, function(t) {
                                    t.isIframe || e(t.image).css("visibility", "hidden"), o._scaleImage(t, {
                                        complete: function(t) {
                                            t.isIframe || e(t.image).css({
                                                opacity: 0,
                                                visibility: "visible"
                                            }).animate({
                                                opacity: 1
                                            }, 200)
                                        }
                                    })
                                })
                            })
                        })), this.$("thumbnails, thumbnails-list").css({
                            overflow: "hidden",
                            position: "relative"
                        }), this.$("image-nav-right, image-nav-left").on("click:fast", function(e) {
                            s.pauseOnInteraction && o.pause();
                            var t = /right/.test(this.className) ? "next" : "prev";
                            o[t]()
                        }).on("click", function(e) {
                            e.preventDefault(), (s.clicknext || s.swipe) && e.stopPropagation()
                        }), e.each(["info", "counter", "image-nav"], function(e, t) {
                            !1 === s["show" + t.substr(0, 1).toUpperCase() + t.substr(1).replace(/-/, "")] && z.moveOut(o.get(t.toLowerCase()))
                        }), this.load(), s.keepSource || v || (this._target.innerHTML = ""), this.get("errors") && this.appendChild("target", "errors"), this.appendChild("target", "container"), s.carousel) {
                        var l = 0,
                            c = s.show;
                        this.bind(i.THUMBNAIL, function() {
                            this.updateCarousel(), ++l == this.getDataLength() && "number" == typeof c && c > 0 && this._carousel.follow(c)
                        })
                    }
                    return s.responsive && r.on("resize", function() {
                        o.isFullscreen() || o.resize()
                    }), s.fullscreenDoubleTap && this.$("stage").on("touchstart", function() {
                        var e, t, i, a, n, r, s = function(e) {
                            return e.originalEvent.touches ? e.originalEvent.touches[0] : e
                        };
                        return o.$("stage").on("touchmove", function() {
                                e = 0
                            }),
                            function(l) {
                                if (!/(-left|-right)/.test(l.target.className)) {
                                    if (r = z.timestamp(), t = s(l).pageX, i = s(l).pageY, l.originalEvent.touches.length < 2 && r - e < 300 && t - a < 20 && i - n < 20) return o.toggleFullscreen(), void l.preventDefault();
                                    e = r, a = t, n = i
                                }
                            }
                    }()), e.each(i.on.binds, function(t, i) {
                        -1 == e.inArray(i.hash, o._binds) && o.bind(i.type, i.callback)
                    }), this
                },
                addTimer: function() {
                    return this._timer.add.apply(this._timer, z.array(arguments)), this
                },
                clearTimer: function() {
                    return this._timer.clear.apply(this._timer, z.array(arguments)), this
                },
                _getWH: function() {
                    var t, i = this.$("container"),
                        a = this.$("target"),
                        n = this,
                        o = {};
                    return e.each(["width", "height"], function(e, r) {
                        n._options[r] && "number" == typeof n._options[r] ? o[r] = n._options[r] : (t = [z.parseValue(i.css(r)), z.parseValue(a.css(r)), i[r](), a[r]()], n["_" + r] || t.splice(t.length, z.parseValue(i.css("min-" + r)), z.parseValue(a.css("min-" + r))), o[r] = f.max.apply(f, t))
                    }), n._userRatio && (o.height = o.width * n._userRatio), o
                },
                _createThumbnails: function(a) {
                    this.get("total").innerHTML = this.getDataLength();
                    var o, r, s, l, c = this,
                        h = this._options,
                        d = a ? this._data.length - a.length : 0,
                        u = d,
                        p = [],
                        f = 0,
                        g = v < 8 ? "http://upload.wikimedia.org/wikipedia/commons/c/c0/Blank.gif" : "data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw%3D%3D",
                        m = function() {
                            var e = c.$("thumbnails").find(".active");
                            return !!e.length && e.find("img").attr("src")
                        }(),
                        y = "string" == typeof h.thumbnails ? h.thumbnails.toLowerCase() : null,
                        w = function(e) {
                            return n.defaultView && n.defaultView.getComputedStyle ? n.defaultView.getComputedStyle(r.container, null)[e] : l.css(e)
                        },
                        b = function(t) {
                            h.pauseOnInteraction && c.pause();
                            var i = e(t.currentTarget).data("index");
                            c.getIndex() !== i && c.show(i), t.preventDefault()
                        },
                        x = function(t, a) {
                            e(t.container).css("visibility", "visible"), c.trigger({
                                type: i.THUMBNAIL,
                                thumbTarget: t.image,
                                index: t.data.order,
                                galleriaData: c.getData(t.data.order)
                            }), "function" == typeof a && a.call(c, t)
                        },
                        $ = function(t, i) {
                            t.scale({
                                width: t.data.width,
                                height: t.data.height,
                                crop: h.thumbCrop,
                                margin: h.thumbMargin,
                                canvas: h.useCanvas,
                                position: h.thumbPosition,
                                complete: function(t) {
                                    var a, n, o = ["left", "top"],
                                        r = ["Width", "Height"];
                                    c.getData(t.index), e.each(r, function(i, r) {
                                        a = r.toLowerCase(), !0 === h.thumbCrop && h.thumbCrop !== a || (n = {}, n[a] = t[a], e(t.container).css(n), n = {}, n[o[i]] = 0, e(t.image).css(n)), t["outer" + r] = e(t.container)["outer" + r](!0)
                                    }), z.toggleQuality(t.image, !0 === h.thumbQuality || "auto" === h.thumbQuality && t.original.width < 3 * t.width), h.thumbDisplayOrder && !t.lazy ? e.each(p, function(e, t) {
                                        if (e === f && t.ready && !t.displayed) return f++, t.displayed = !0, void x(t, i)
                                    }) : x(t, i)
                                }
                            })
                        };
                    for (a || (this._thumbnails = [], this.$("thumbnails").empty()); this._data[d]; d++) s = this._data[d], o = s.thumb || s.image, !0 !== h.thumbnails && "lazy" != y || !s.thumb && !s.image ? s.iframe && null !== y || "empty" === y || "numbers" === y ? (r = {
                        container: z.create("galleria-image"),
                        image: z.create("img", "span"),
                        ready: !0,
                        data: {
                            order: d
                        }
                    }, "numbers" === y && e(r.image).text(d + 1), s.iframe && e(r.image).addClass("iframe"), this.$("thumbnails").append(r.container), t.setTimeout(function(t, a, n) {
                        return function() {
                            e(n).append(t), c.trigger({
                                type: i.THUMBNAIL,
                                thumbTarget: t,
                                index: a,
                                galleriaData: c.getData(a)
                            })
                        }
                    }(r.image, d, r.container), 50 + 20 * d)) : r = {
                        container: null,
                        image: null
                    } : (r = new i.Picture(d), r.index = d, r.displayed = !1, r.lazy = !1, r.video = !1, this.$("thumbnails").append(r.container), l = e(r.container), l.css("visibility", "hidden"), r.data = {
                        width: z.parseValue(w("width")),
                        height: z.parseValue(w("height")),
                        order: d,
                        src: o
                    }, !0 !== h.thumbCrop ? l.css({
                        width: "auto",
                        height: "auto"
                    }) : l.css({
                        width: r.data.width,
                        height: r.data.height
                    }), "lazy" == y ? (l.addClass("lazy"), r.lazy = !0, r.load(g, {
                        height: r.data.height,
                        width: r.data.width
                    })) : r.load(o, $), "all" === h.preload && r.preload(s.image)), e(r.container).add(h.keepSource && h.linkSourceImages ? s.original : null).data("index", d).on(h.thumbEventType, b).data("thumbload", $), m === o && e(r.container).addClass("active"), this._thumbnails.push(r);
                    return p = this._thumbnails.slice(u), this
                },
                lazyLoad: function(t, i) {
                    var a = t.constructor == Array ? t : [t],
                        n = this,
                        o = 0;
                    return e.each(a, function(t, r) {
                        if (!(r > n._thumbnails.length - 1)) {
                            var s = n._thumbnails[r],
                                l = s.data,
                                c = function() {
                                    ++o == a.length && "function" == typeof i && i.call(n)
                                },
                                h = e(s.container).data("thumbload");
                            s.video ? h.call(n, s, c) : s.load(l.src, function(e) {
                                h.call(n, e, c)
                            })
                        }
                    }), this
                },
                lazyLoadChunks: function(e, i) {
                    var a = this.getDataLength(),
                        n = 0,
                        o = 0,
                        r = [],
                        s = [],
                        l = this;
                    for (i = i || 0; n < a; n++) s.push(n), ++o != e && n != a - 1 || (r.push(s), o = 0, s = []);
                    var c = function(e) {
                        var a = r.shift();
                        a && t.setTimeout(function() {
                            l.lazyLoad(a, function() {
                                c(!0)
                            })
                        }, i && e ? i : 0)
                    };
                    return c(!1), this
                },
                _run: function() {
                    var n = this;
                    n._createThumbnails(), z.wait({
                        timeout: 1e4,
                        until: function() {
                            return i.OPERA && n.$("stage").css("display", "inline-block"), n._stageWidth = n.$("stage").width(), n._stageHeight = n.$("stage").height(), n._stageWidth && n._stageHeight > 50
                        },
                        success: function() {
                            if (I.push(n), n._options.swipe) {
                                var o = n.$("images").width(n.getDataLength() * n._stageWidth);
                                e.each(new Array(n.getDataLength()), function(t) {
                                    var a = new i.Picture,
                                        r = n.getData(t);
                                    e(a.container).css({
                                        position: "absolute",
                                        top: 0,
                                        left: n._stageWidth * t
                                    }).prepend(n._layers[t] = e(z.create("galleria-layer")).css({
                                        position: "absolute",
                                        top: 0,
                                        left: 0,
                                        right: 0,
                                        bottom: 0,
                                        zIndex: 2
                                    })[0]).appendTo(o), r.video && P(a.container), n._controls.slides.push(a);
                                    var s = new i.Picture;
                                    s.isIframe = !0, e(s.container).attr("class", "galleria-frame").css({
                                        position: "absolute",
                                        top: 0,
                                        left: 0,
                                        zIndex: 4,
                                        background: "#000",
                                        display: "none"
                                    }).appendTo(a.container), n._controls.frames.push(s)
                                }), n.finger.setup()
                            }
                            if (z.show(n.get("counter")), n._options.carousel && n._carousel.bindControls(), n._options.autoplay && (n.pause(), "number" == typeof n._options.autoplay && (n._playtime = n._options.autoplay), n._playing = !0), n._firstrun) return n._options.autoplay && n.trigger(i.PLAY), void("number" == typeof n._options.show && n.show(n._options.show));
                            n._firstrun = !0, i.History && i.History.change(function(e) {
                                isNaN(e) ? t.history.go(-1) : n.show(e, a, !0)
                            }), n.trigger(i.READY), n.theme.init.call(n, n._options), e.each(i.ready.callbacks, function(e, t) {
                                "function" == typeof t && t.call(n, n._options)
                            }), n._options.extend.call(n, n._options), /^[0-9]{1,4}$/.test(u) && i.History ? n.show(u, a, !0) : n._data[n._options.show] && n.show(n._options.show), n._options.autoplay && n.trigger(i.PLAY)
                        },
                        error: function() {
                            i.raise("Stage width or height is too small to show the gallery. Traced measures: width:" + n._stageWidth + "px, height: " + n._stageHeight + "px.", !0)
                        }
                    })
                },
                load: function(t, a, n) {
                    var o = this,
                        r = this._options;
                    return this._data = [], this._thumbnails = [], this.$("thumbnails").empty(), "function" == typeof a && (n = a, a = null), t = t || r.dataSource, a = a || r.dataSelector, n = n || r.dataConfig, e.isPlainObject(t) && (t = [t]), e.isArray(t) ? this.validate(t) ? this._data = t : i.raise("Load failed: JSON Array not valid.") : (a += ",.video,.iframe", e(t).find(a).each(function(t, i) {
                        i = e(i);
                        var a = {},
                            r = i.parent(),
                            s = r.attr("href"),
                            l = r.attr("rel");
                        s && ("IMG" == i[0].nodeName || i.hasClass("video")) && T(s) ? a.video = s : s && i.hasClass("iframe") ? a.iframe = s : a.image = a.big = s, l && (a.big = l), e.each("big title description link layer image".split(" "), function(e, t) {
                            i.data(t) && (a[t] = i.data(t).toString())
                        }), a.big || (a.big = a.image), o._data.push(e.extend({
                            title: i.attr("title") || "",
                            thumb: i.attr("src"),
                            image: i.attr("src"),
                            big: i.attr("src"),
                            description: i.attr("alt") || "",
                            link: i.attr("longdesc"),
                            original: i.get(0)
                        }, a, n(i)))
                    })), "function" == typeof r.dataSort ? s.sort.call(this._data, r.dataSort) : "random" == r.dataSort && this._data.sort(function() {
                        return f.round(f.random()) - .5
                    }), this.getDataLength() && this._parseData(function() {
                        this.trigger(i.DATA)
                    }), this
                },
                _parseData: function(t) {
                    var i, n = this,
                        o = !1,
                        r = function() {
                            var i = !0;
                            e.each(n._data, function(e, t) {
                                if (t.loading) return i = !1, !1
                            }), i && !o && (o = !0, t.call(n))
                        };
                    return e.each(this._data, function(t, o) {
                        if (i = n._data[t], "thumb" in o == 0 && (i.thumb = o.image), o.big || (i.big = o.image), "video" in o) {
                            var s = T(o.video);
                            s && (i.iframe = new k(s.provider, s.id).embed() + function() {
                                if ("object" == typeof n._options[s.provider]) {
                                    var t = [];
                                    return e.each(n._options[s.provider], function(e, i) {
                                        t.push(e + "=" + i)
                                    }), "youtube" == s.provider && (t = ["wmode=opaque"].concat(t)), "?" + t.join("&")
                                }
                                return ""
                            }(), i.thumb && i.image || e.each(["thumb", "image"], function(e, t) {
                                if ("image" == t && !n._options.videoPoster) return void(i.image = a);
                                var o = new k(s.provider, s.id);
                                i[t] || (i.loading = !0, o.getMedia(t, function(e, t) {
                                    return function(i) {
                                        e[t] = i, "image" != t || e.big || (e.big = e.image), delete e.loading, r()
                                    }
                                }(i, t)))
                            }))
                        }
                    }), r(), this
                },
                destroy: function() {
                    return this.$("target").data("galleria", null), this.$("container").off("galleria"), this.get("target").innerHTML = this._original.html, this.clearTimer(), z.removeFromArray(S, this), z.removeFromArray(I, this), i._waiters.length && e.each(i._waiters, function(e, i) {
                        i && t.clearTimeout(i)
                    }), this
                },
                splice: function() {
                    var e = this,
                        i = z.array(arguments);
                    return t.setTimeout(function() {
                        s.splice.apply(e._data, i), e._parseData(function() {
                            e._createThumbnails()
                        })
                    }, 2), e
                },
                push: function() {
                    var e = this,
                        i = z.array(arguments);
                    return 1 == i.length && i[0].constructor == Array && (i = i[0]), t.setTimeout(function() {
                        s.push.apply(e._data, i), e._parseData(function() {
                            e._createThumbnails(i)
                        })
                    }, 2), e
                },
                _getActive: function() {
                    return this._controls.getActive()
                },
                validate: function(e) {
                    return !0
                },
                bind: function(e, t) {
                    return e = _(e), this.$("container").on(e, this.proxy(t)), this
                },
                unbind: function(e) {
                    return e = _(e), this.$("container").off(e), this
                },
                trigger: function(t) {
                    return t = "object" == typeof t ? e.extend(t, {
                        scope: this
                    }) : {
                        type: _(t),
                        scope: this
                    }, this.$("container").trigger(t), this
                },
                addIdleState: function(e, t, i, a) {
                    return this._idle.add.apply(this._idle, z.array(arguments)), this
                },
                removeIdleState: function(e) {
                    return this._idle.remove.apply(this._idle, z.array(arguments)), this
                },
                enterIdleMode: function() {
                    return this._idle.hide(), this
                },
                exitIdleMode: function() {
                    return this._idle.showAll(), this
                },
                enterFullscreen: function(e) {
                    return this._fullscreen.enter.apply(this, z.array(arguments)), this
                },
                exitFullscreen: function(e) {
                    return this._fullscreen.exit.apply(this, z.array(arguments)), this
                },
                toggleFullscreen: function(e) {
                    return this._fullscreen[this.isFullscreen() ? "exit" : "enter"].apply(this, z.array(arguments)), this
                },
                bindTooltip: function(e, t) {
                    return this._tooltip.bind.apply(this._tooltip, z.array(arguments)), this
                },
                defineTooltip: function(e, t) {
                    return this._tooltip.define.apply(this._tooltip, z.array(arguments)), this
                },
                refreshTooltip: function(e) {
                    return this._tooltip.show.apply(this._tooltip, z.array(arguments)), this
                },
                openLightbox: function() {
                    return this._lightbox.show.apply(this._lightbox, z.array(arguments)), this
                },
                closeLightbox: function() {
                    return this._lightbox.hide.apply(this._lightbox, z.array(arguments)), this
                },
                hasVariation: function(t) {
                    return e.inArray(t, this._options.variation.split(/\s+/)) > -1
                },
                getActiveImage: function() {
                    var e = this._getActive();
                    return e ? e.image : a
                },
                getActiveThumb: function() {
                    return this._thumbnails[this._active].image || a
                },
                getMousePosition: function(e) {
                    return {
                        x: e.pageX - this.$("container").offset().left,
                        y: e.pageY - this.$("container").offset().top
                    }
                },
                addPan: function(t) {
                    if (!1 !== this._options.imageCrop) {
                        t = e(t || this.getActiveImage());
                        var i = this,
                            a = t.width() / 2,
                            n = t.height() / 2,
                            o = parseInt(t.css("left"), 10),
                            r = parseInt(t.css("top"), 10),
                            s = o || 0,
                            l = r || 0,
                            c = 0,
                            h = 0,
                            d = !1,
                            u = z.timestamp(),
                            p = 0,
                            g = 0,
                            m = function(e, i, a) {
                                if (e > 0 && (g = f.round(f.max(-1 * e, f.min(0, i))), p !== g))
                                    if (p = g, 8 === v) t.parent()["scroll" + a](-1 * g);
                                    else {
                                        var n = {};
                                        n[a.toLowerCase()] = g, t.css(n)
                                    }
                            },
                            y = function(e) {
                                z.timestamp() - u < 50 || (d = !0, a = i.getMousePosition(e).x, n = i.getMousePosition(e).y)
                            },
                            w = function(e) {
                                d && (c = t.width() - i._stageWidth, h = t.height() - i._stageHeight, o = a / i._stageWidth * c * -1, r = n / i._stageHeight * h * -1, s += (o - s) / i._options.imagePanSmoothness, l += (r - l) / i._options.imagePanSmoothness, m(h, l, "Top"), m(c, s, "Left"))
                            };
                        return 8 === v && (t.parent().scrollTop(-1 * l).scrollLeft(-1 * s), t.css({
                            top: 0,
                            left: 0
                        })), this.$("stage").off("mousemove", y).on("mousemove", y), this.addTimer("pan" + i._id, w, 50, !0), this
                    }
                },
                proxy: function(e, t) {
                    return "function" != typeof e ? g : (t = t || this, function() {
                        return e.apply(t, z.array(arguments))
                    })
                },
                getThemeName: function() {
                    return this.theme.name
                },
                removePan: function() {
                    return this.$("stage").off("mousemove"), this.clearTimer("pan" + this._id), this
                },
                addElement: function(t) {
                    var i = this._dom;
                    return e.each(z.array(arguments), function(e, t) {
                        i[t] = z.create("galleria-" + t)
                    }), this
                },
                attachKeyboard: function(e) {
                    return this._keyboard.attach.apply(this._keyboard, z.array(arguments)), this
                },
                detachKeyboard: function() {
                    return this._keyboard.detach.apply(this._keyboard, z.array(arguments)), this
                },
                appendChild: function(e, t) {
                    return this.$(e).append(this.get(t) || t), this
                },
                prependChild: function(e, t) {
                    return this.$(e).prepend(this.get(t) || t), this
                },
                remove: function(e) {
                    return this.$(z.array(arguments).join(",")).remove(), this
                },
                append: function(e) {
                    var t, i;
                    for (t in e)
                        if (e.hasOwnProperty(t))
                            if (e[t].constructor === Array)
                                for (i = 0; e[t][i]; i++) this.appendChild(t, e[t][i]);
                            else this.appendChild(t, e[t]);
                    return this
                },
                _scaleImage: function(t, i) {
                    if (t = t || this._controls.getActive()) {
                        var a, n = function(t) {
                            e(t.container).children(":first").css({
                                top: f.max(0, z.parseValue(t.image.style.top)),
                                left: f.max(0, z.parseValue(t.image.style.left)),
                                width: z.parseValue(t.image.width),
                                height: z.parseValue(t.image.height)
                            })
                        };
                        return i = e.extend({
                            width: this._stageWidth,
                            height: this._stageHeight,
                            crop: this._options.imageCrop,
                            max: this._options.maxScaleRatio,
                            min: this._options.minScaleRatio,
                            margin: this._options.imageMargin,
                            position: this._options.imagePosition,
                            iframelimit: this._options.maxVideoSize
                        }, i), this._options.layerFollow && !0 !== this._options.imageCrop ? "function" == typeof i.complete ? (a = i.complete, i.complete = function() {
                            a.call(t, t), n(t)
                        }) : i.complete = n : e(t.container).children(":first").css({
                            top: 0,
                            left: 0
                        }), t.scale(i), this
                    }
                },
                updateCarousel: function() {
                    return this._carousel.update(), this
                },
                resize: function(t, i) {
                    "function" == typeof t && (i = t, t = a), t = e.extend({
                        width: 0,
                        height: 0
                    }, t);
                    var n = this,
                        o = this.$("container");
                    return e.each(t, function(e, i) {
                        i || (o[e]("auto"), t[e] = n._getWH()[e])
                    }), e.each(t, function(e, t) {
                        o[e](t)
                    }), this.rescale(i)
                },
                rescale: function(t, n, o) {
                    var r = this;
                    return "function" == typeof t && (o = t, t = a),
                        function() {
                            r._stageWidth = t || r.$("stage").width(),
                                r._stageHeight = n || r.$("stage").height(), r._options.swipe ? (e.each(r._controls.slides, function(t, i) {
                                    r._scaleImage(i), e(i.container).css("left", r._stageWidth * t)
                                }), r.$("images").css("width", r._stageWidth * r.getDataLength())) : r._scaleImage(), r._options.carousel && r.updateCarousel(), r._controls.frames[r._controls.active] && r._controls.frames[r._controls.active].scale({
                                    width: r._stageWidth,
                                    height: r._stageHeight,
                                    iframelimit: r._options.maxVideoSize
                                }), r.trigger(i.RESCALE), "function" == typeof o && o.call(r)
                        }.call(r), this
                },
                refreshImage: function() {
                    return this._scaleImage(), this._options.imagePan && this.addPan(), this
                },
                _preload: function() {
                    if (this._options.preload) {
                        var e, t, a, n = this.getNext();
                        try {
                            for (t = this._options.preload; t > 0; t--) e = new i.Picture, a = this.getData(n), e.preload(this.isFullscreen() && a.big ? a.big : a.image), n = this.getNext(n)
                        } catch (e) {}
                    }
                },
                show: function(a, n, o) {
                    var r = this._options.swipe;
                    if (r || !(this._queue.length > 3 || !1 === a || !this._options.queue && this._queue.stalled)) {
                        if (a = f.max(0, f.min(parseInt(a, 10), this.getDataLength() - 1)), n = void 0 !== n ? !!n : a < this.getIndex(), !(o = o || !1) && i.History) return void i.History.set(a.toString());
                        if (this.finger && a !== this._active && (this.finger.to = -a * this.finger.width, this.finger.index = a), this._active = a, r) {
                            var l = this.getData(a),
                                c = this;
                            if (!l) return;
                            var h = this.isFullscreen() && l.big ? l.big : l.image || l.iframe,
                                d = this._controls.slides[a],
                                u = d.isCached(h),
                                p = this._thumbnails[a],
                                g = {
                                    cached: u,
                                    index: a,
                                    rewind: n,
                                    imageTarget: d.image,
                                    thumbTarget: p.image,
                                    galleriaData: l
                                };
                            this.trigger(e.extend(g, {
                                type: i.LOADSTART
                            })), c.$("container").removeClass("videoplay");
                            var m = function() {
                                c._layers[a].innerHTML = c.getData().layer || "", c.trigger(e.extend(g, {
                                    type: i.LOADFINISH
                                })), c._playCheck()
                            };
                            c._preload(), t.setTimeout(function() {
                                d.ready && e(d.image).attr("src") == h ? (c.trigger(e.extend(g, {
                                    type: i.IMAGE
                                })), m()) : (l.iframe && !l.image && (d.isIframe = !0), d.load(h, function(t) {
                                    g.imageTarget = t.image, c._scaleImage(t, m).trigger(e.extend(g, {
                                        type: i.IMAGE
                                    })), m()
                                }))
                            }, 100)
                        } else s.push.call(this._queue, {
                            index: a,
                            rewind: n
                        }), this._queue.stalled || this._show();
                        return this
                    }
                },
                _show: function() {
                    var n = this,
                        o = this._queue[0],
                        r = this.getData(o.index);
                    if (r) {
                        var l = this.isFullscreen() && r.big ? r.big : r.image || r.iframe,
                            c = this._controls.getActive(),
                            h = this._controls.getNext(),
                            d = h.isCached(l),
                            u = this._thumbnails[o.index],
                            p = function() {
                                e(h.image).trigger("mouseup")
                            };
                        n.$("container").toggleClass("iframe", !!r.isIframe).removeClass("videoplay");
                        var f = function(o, r, l, c, h) {
                            return function() {
                                var d;
                                H.active = !1, z.toggleQuality(r.image, n._options.imageQuality), n._layers[n._controls.active].innerHTML = "", e(l.container).css({
                                    zIndex: 0,
                                    opacity: 0
                                }).show(), e(l.container).find("iframe, .galleria-videoicon").remove(), e(n._controls.frames[n._controls.active].container).hide(), e(r.container).css({
                                    zIndex: 1,
                                    left: 0,
                                    top: 0
                                }).show(), n._controls.swap(), n._options.imagePan && n.addPan(r.image), (o.iframe && o.image || o.link || n._options.lightbox || n._options.clicknext) && e(r.image).css({
                                    cursor: "pointer"
                                }).on("mouseup", function(r) {
                                    if (!("number" == typeof r.which && r.which > 1)) {
                                        if (o.iframe) {
                                            n.isPlaying() && n.pause();
                                            var s = n._controls.frames[n._controls.active],
                                                l = n._stageWidth,
                                                c = n._stageHeight;
                                            return e(s.container).css({
                                                width: l,
                                                height: c,
                                                opacity: 0
                                            }).show().animate({
                                                opacity: 1
                                            }, 200), void t.setTimeout(function() {
                                                s.load(o.iframe + (o.video ? "&autoplay=1" : ""), {
                                                    width: l,
                                                    height: c
                                                }, function(e) {
                                                    n.$("container").addClass("videoplay"), e.scale({
                                                        width: n._stageWidth,
                                                        height: n._stageHeight,
                                                        iframelimit: o.video ? n._options.maxVideoSize : a
                                                    })
                                                })
                                            }, 100)
                                        }
                                        return n._options.clicknext && !i.TOUCH ? (n._options.pauseOnInteraction && n.pause(), void n.next()) : o.link ? void(n._options.popupLinks ? d = t.open(o.link, "_blank") : t.location.href = o.link) : void(n._options.lightbox && n.openLightbox())
                                    }
                                }), n._playCheck(), n.trigger({
                                    type: i.IMAGE,
                                    index: c.index,
                                    imageTarget: r.image,
                                    thumbTarget: h.image,
                                    galleriaData: o
                                }), s.shift.call(n._queue), n._queue.stalled = !1, n._queue.length && n._show()
                            }
                        }(r, h, c, o, u);
                        this._options.carousel && this._options.carouselFollow && this._carousel.follow(o.index), n._preload(), z.show(h.container), h.isIframe = r.iframe && !r.image, e(n._thumbnails[o.index].container).addClass("active").siblings(".active").removeClass("active"), n.trigger({
                            type: i.LOADSTART,
                            cached: d,
                            index: o.index,
                            rewind: o.rewind,
                            imageTarget: h.image,
                            thumbTarget: u.image,
                            galleriaData: r
                        }), n._queue.stalled = !0, h.load(l, function(t) {
                            var s = e(n._layers[1 - n._controls.active]).html(r.layer || "").hide();
                            n._scaleImage(t, {
                                complete: function(t) {
                                    "image" in c && z.toggleQuality(c.image, !1), z.toggleQuality(t.image, !1), n.removePan(), n.setInfo(o.index), n.setCounter(o.index), r.layer && (s.show(), (r.iframe && r.image || r.link || n._options.lightbox || n._options.clicknext) && s.css("cursor", "pointer").off("mouseup").mouseup(p)), r.video && r.image && P(t.container);
                                    var l = n._options.transition;
                                    if (e.each({
                                            initial: null === c.image,
                                            touch: i.TOUCH,
                                            fullscreen: n.isFullscreen()
                                        }, function(e, t) {
                                            if (t && n._options[e + "Transition"] !== a) return l = n._options[e + "Transition"], !1
                                        }), l in H.effects == 0) f();
                                    else {
                                        var h = {
                                            prev: c.container,
                                            next: t.container,
                                            rewind: o.rewind,
                                            speed: n._options.transitionSpeed || 400
                                        };
                                        H.active = !0, H.init.call(n, l, h, f)
                                    }
                                    n.trigger({
                                        type: i.LOADFINISH,
                                        cached: d,
                                        index: o.index,
                                        rewind: o.rewind,
                                        imageTarget: t.image,
                                        thumbTarget: n._thumbnails[o.index].image,
                                        galleriaData: n.getData(o.index)
                                    })
                                }
                            })
                        })
                    }
                },
                getNext: function(e) {
                    return e = "number" == typeof e ? e : this.getIndex(), e === this.getDataLength() - 1 ? 0 : e + 1
                },
                getPrev: function(e) {
                    return e = "number" == typeof e ? e : this.getIndex(), 0 === e ? this.getDataLength() - 1 : e - 1
                },
                next: function() {
                    return this.getDataLength() > 1 && this.show(this.getNext(), !1), this
                },
                prev: function() {
                    return this.getDataLength() > 1 && this.show(this.getPrev(), !0), this
                },
                get: function(e) {
                    return e in this._dom ? this._dom[e] : null
                },
                getData: function(e) {
                    return e in this._data ? this._data[e] : this._data[this._active]
                },
                getDataLength: function() {
                    return this._data.length
                },
                getIndex: function() {
                    return "number" == typeof this._active && this._active
                },
                getStageHeight: function() {
                    return this._stageHeight
                },
                getStageWidth: function() {
                    return this._stageWidth
                },
                getOptions: function(e) {
                    return void 0 === e ? this._options : this._options[e]
                },
                setOptions: function(t, i) {
                    return "object" == typeof t ? e.extend(this._options, t) : this._options[t] = i, this
                },
                play: function(e) {
                    return this._playing = !0, this._playtime = e || this._playtime, this._playCheck(), this.trigger(i.PLAY), this
                },
                pause: function() {
                    return this._playing = !1, this.trigger(i.PAUSE), this
                },
                playToggle: function(e) {
                    return this._playing ? this.pause() : this.play(e)
                },
                isPlaying: function() {
                    return this._playing
                },
                isFullscreen: function() {
                    return this._fullscreen.active
                },
                _playCheck: function() {
                    var e = this,
                        t = 0,
                        a = z.timestamp(),
                        n = "play" + this._id;
                    if (this._playing) {
                        this.clearTimer(n);
                        var o = function() {
                            if ((t = z.timestamp() - a) >= e._playtime && e._playing) return e.clearTimer(n), void e.next();
                            e._playing && (e.trigger({
                                type: i.PROGRESS,
                                percent: f.ceil(t / e._playtime * 100),
                                seconds: f.floor(t / 1e3),
                                milliseconds: t
                            }), e.addTimer(n, o, 20))
                        };
                        e.addTimer(n, o, 20)
                    }
                },
                setPlaytime: function(e) {
                    return this._playtime = e, this
                },
                setIndex: function(e) {
                    return this._active = e, this
                },
                setCounter: function(e) {
                    if ("number" == typeof e ? e++ : void 0 === e && (e = this.getIndex() + 1), this.get("current").innerHTML = e, v) {
                        var t = this.$("counter"),
                            i = t.css("opacity");
                        1 === parseInt(i, 10) ? z.removeAlpha(t[0]) : this.$("counter").css("opacity", i)
                    }
                    return this
                },
                setInfo: function(t) {
                    var i = this,
                        a = this.getData(t);
                    return e.each(["title", "description"], function(e, t) {
                        var n = i.$("info-" + t);
                        a[t] ? n[a[t].length ? "show" : "hide"]().html(a[t]) : n.empty().hide()
                    }), this
                },
                hasInfo: function(e) {
                    var t, i = "title description".split(" ");
                    for (t = 0; i[t]; t++)
                        if (this.getData(e)[i[t]]) return !0;
                    return !1
                },
                jQuery: function(t) {
                    var i = this,
                        a = [];
                    e.each(t.split(","), function(t, n) {
                        n = e.trim(n), i.get(n) && a.push(n)
                    });
                    var n = e(i.get(a.shift()));
                    return e.each(a, function(e, t) {
                        n = n.add(i.get(t))
                    }), n
                },
                $: function(e) {
                    return this.jQuery.apply(this, z.array(arguments))
                }
            }, e.each(x, function(e, t) {
                var a = /_/.test(t) ? t.replace(/_/g, "") : t;
                i[t.toUpperCase()] = "galleria." + a
            }),
            e.extend(i, {
                IE9: 9 === v,
                IE8: 8 === v,
                IE7: 7 === v,
                IE6: 6 === v,
                IE: v,
                WEBKIT: /webkit/.test(d),
                CHROME: /chrome/.test(d),
                SAFARI: /safari/.test(d) && !/chrome/.test(d),
                QUIRK: v && n.compatMode && "BackCompat" === n.compatMode,
                MAC: /mac/.test(navigator.platform.toLowerCase()),
                OPERA: !!t.opera,
                IPHONE: /iphone/.test(d),
                IPAD: /ipad/.test(d),
                ANDROID: /android/.test(d),
                TOUCH: "ontouchstart" in n
            }),
            //i.addTheme = function (a) {
            //a.name || i.raise("No theme name specified"), "object" != typeof a.defaults ? a.defaults = {} : a.defaults = $(a.defaults); var n, o = !1;
            //return "string" == typeof a.css ? (e("link").each(function (e, t) {
            //    if (n = new RegExp(a.css), n.test(t.href)) return o = !0, O(a), !1
            //}), o || e(function () {
            //    var r = 0, s = function () {
            //        e("script").each(function (e, i) {
            //            n = new RegExp("galleria\\." + a.name.toLowerCase() + "\\."), n.test(i.src) && (o = i.src.replace(/[^\/]*$/, "") + a.css,
            //            t.setTimeout(function () { z.loadCSS(o, "galleria-theme-" + a.name, function () { O(a) }) }, 1))
            //        }), o || (r++ > 5 ? i.raise("No theme CSS loaded") : t.setTimeout(s, 500))
            //    }; s()
            //})) : O(a), a
            //},
            //i.loadTheme = function (a, n) {
            //    if (!e("script").filter(function () {
            //        return e(this).attr("src") == a
            //    }).length) { var o, r = !1; return e(t).load(function () { r || (o = t.setTimeout(function () { r || i.raise("Galleria had problems loading theme at " + a + ". Please check theme path or load manually.", !0) }, 2e4)) }), z.loadScript(a, function () { r = !0, t.clearTimeout(o) }), i }
            //},
            i.get = function(e) {
                return S[e] ? S[e] : "number" != typeof e ? S : void i.raise("Gallery index " + e + " not found")
            }, i.configure = function(t, a) {
                var n = {};
                return "string" == typeof t && a ? (n[t] = a, t = n) : e.extend(n, t), i.configure.options = n, e.each(i.get(), function(e, t) {
                    t.setOptions(n)
                }), i
            }, i.configure.options = {}, i.on = function(t, a) {
                if (t) {
                    a = a || g;
                    var n = t + a.toString().replace(/\s/g, "") + z.timestamp();
                    return e.each(i.get(), function(e, i) {
                        i._binds.push(n), i.bind(t, a)
                    }), i.on.binds.push({
                        type: t,
                        callback: a,
                        hash: n
                    }), i
                }
            }, i.on.binds = [], i.run = function(t, a) {
                return e.isFunction(a) && (a = {
                    extend: a
                }), e(t || "#galleria").galleria(a), i
            }, i.addTransition = function(e, t) {
                return H.effects[e] = t, i
            }, i.utils = z, i.log = function() {
                var i = z.array(arguments);
                if (!("console" in t && "log" in t.console)) return t.alert(i.join("<br>"));
                try {
                    return t.console.log.apply(t.console, i)
                } catch (a) {
                    e.each(i, function() {
                        t.console.log(this)
                    })
                }
            }, i.ready = function(t) {
                return "function" != typeof t ? i : (e.each(I, function(e, i) {
                    t.call(i, i._options)
                }), i.ready.callbacks.push(t), i)
            }, i.ready.callbacks = [], i.raise = function(t, i) {
                var a = i ? "Fatal error" : "Error",
                    n = {
                        color: "#fff",
                        position: "absolute",
                        top: 0,
                        left: 0,
                        zIndex: 1e5
                    },
                    o = function(t) {
                        var o = '<div style="padding:4px;margin:0 0 2px;background:#' + (i ? "811" : "222") + ';">' + (i ? "<strong>" + a + ": </strong>" : "") + t + "</div>";
                        e.each(S, function() {
                            var e = this.$("errors"),
                                t = this.$("target");
                            e.length || (t.css("position", "relative"), e = this.addElement("errors").appendChild("target", "errors").$("errors").css(n)), e.append(o)
                        }), S.length || e("<div>").css(e.extend(n, {
                            position: "fixed"
                        })).append(o).appendTo(y().body)
                    };
                if (l) {
                    if (o(t), i) throw new Error(a + ": " + t)
                } else if (i) {
                    if (A) return;
                    A = !0, i = !1, o("Gallery could not load.")
                }
            }, i.version = 1.41, i.getLoadedThemes = function() {
                return e.map(M, function(e) {
                    return e.name
                })
            }, i.requires = function(e, t) {
                return t = t || "You need to upgrade Galleria to version " + e + " to use one or more components.", i.version < e && i.raise(t, !0), i
            },
            i.Picture = function(t) {
                this.id = t || null, this.image = null, this.container = z.create("galleria-image"), e(this.container).css({
                        overflow: "hidden",
                        position: "relative"
                    }),
                    this.original = {
                        width: 0,
                        height: 0
                    }, this.ready = !1, this.isIframe = !1
            }, i.Picture.prototype = {
                cache: {},
                show: function() {
                    z.show(this.image)
                },
                hide: function() {
                    z.moveOut(this.image)
                },
                clear: function() {
                    this.image = null
                },
                isCached: function(e) {
                    return !!this.cache[e]
                },
                preload: function(t) {
                    e(new Image).load(function(e, t) {
                        return function() {
                            t[e] = e
                        }
                    }(t, this.cache)).attr("src", t)
                },
                load: function(a, n, o) {
                    if ("function" == typeof n && (o = n, n = null), this.isIframe) {
                        var r = "if" + (new Date).getTime(),
                            s = this.image = e("<iframe>", {
                                src: a,
                                frameborder: 0,
                                id: r,
                                allowfullscreen: !0,
                                css: {
                                    visibility: "hidden"
                                }
                            })[0];
                        return n && e(s).css(n), e(this.container).find("iframe,img").remove(), this.container.appendChild(this.image), e("#" + r).load(function(i, a) {
                            return function() {
                                t.setTimeout(function() {
                                    e(i.image).css("visibility", "visible"), "function" == typeof a && a.call(i, i)
                                }, 10)
                            }
                        }(this, o)), this.container
                    }
                    this.image = new Image, i.IE8 && e(this.image).css("filter", "inherit"), i.IE || i.CHROME || i.SAFARI || e(this.image).css("image-rendering", "optimizequality");
                    var l = !1,
                        c = !1,
                        d = e(this.container),
                        u = e(this.image),
                        p = function() {
                            l ? h ? e(this).attr("src", h) : i.raise("Image not found: " + a) : (l = !0, t.setTimeout(function(e, t) {
                                return function() {
                                    e.attr("src", t + (t.indexOf("?") > -1 ? "&" : "?") + z.timestamp())
                                }
                            }(e(this), a), 50))
                        },
                        f = function(a, o, r) {
                            return function() {
                                var s = function() {
                                    e(this).off("load"), a.original = n || {
                                        height: this.height,
                                        width: this.width
                                    }, i.HAS3D && (this.style.MozTransform = this.style.webkitTransform = "translate3d(0,0,0)"), d.append(this), a.cache[r] = r, "function" == typeof o && t.setTimeout(function() {
                                        o.call(a, a)
                                    }, 1)
                                };
                                this.width && this.height ? s.call(this) : function(t) {
                                    z.wait({
                                        until: function() {
                                            return t.width && t.height
                                        },
                                        success: function() {
                                            s.call(t)
                                        },
                                        error: function() {
                                            c ? i.raise("Could not extract width/height from image: " + t.src + ". Traced measures: width:" + t.width + "px, height: " + t.height + "px.") : (e(new Image).load(f).attr("src", t.src), c = !0)
                                        },
                                        timeout: 100
                                    })
                                }(this)
                            }
                        }(this, o, a);
                    return d.find("iframe,img").remove(), u.css("display", "block"), z.hide(this.image), e.each("minWidth minHeight maxWidth maxHeight".split(" "), function(e, t) {
                        u.css(t, /min/.test(t) ? "0" : "none")
                    }), u.load(f).on("error", p).attr("src", a), this.container
                },
                scale: function(t) {
                    var n = this;
                    if (t = e.extend({
                            width: 0,
                            height: 0,
                            min: a,
                            max: a,
                            margin: 0,
                            complete: g,
                            position: "center",
                            crop: !1,
                            canvas: !1,
                            iframelimit: a
                        }, t), this.isIframe) {
                        var o, r, s = t.width,
                            l = t.height;
                        if (t.iframelimit) {
                            var c = f.min(t.iframelimit / s, t.iframelimit / l);
                            c < 1 ? (o = s * c, r = l * c, e(this.image).css({
                                top: l / 2 - r / 2,
                                left: s / 2 - o / 2,
                                position: "absolute"
                            })) : e(this.image).css({
                                top: 0,
                                left: 0
                            })
                        }
                        e(this.image).width(o || s).height(r || l).removeAttr("width").removeAttr("height"), e(this.container).width(s).height(l), t.complete.call(n, n);
                        try {
                            this.image.contentWindow && e(this.image.contentWindow).trigger("resize")
                        } catch (e) {}
                        return this.container
                    }
                    if (!this.image) return this.container;
                    var h, d, u, p = e(n.container);
                    return z.wait({
                        until: function() {
                            return h = t.width || p.width() || z.parseValue(p.css("width")), d = t.height || p.height() || z.parseValue(p.css("height")), h && d
                        },
                        success: function() {
                            var i = (h - 2 * t.margin) / n.original.width,
                                a = (d - 2 * t.margin) / n.original.height,
                                o = f.min(i, a),
                                r = f.max(i, a),
                                s = {
                                    true: r,
                                    width: i,
                                    height: a,
                                    false: o,
                                    landscape: n.original.width > n.original.height ? r : o,
                                    portrait: n.original.width < n.original.height ? r : o
                                },
                                l = s[t.crop.toString()],
                                c = "";
                            t.max && (l = f.min(t.max, l)), t.min && (l = f.max(t.min, l)), e.each(["width", "height"], function(t, i) {
                                e(n.image)[i](n[i] = n.image[i] = f.round(n.original[i] * l))
                            }), e(n.container).width(h).height(d), t.canvas && E && (E.elem.width = n.width, E.elem.height = n.height, c = n.image.src + ":" + n.width + "x" + n.height, n.image.src = E.cache[c] || function(e) {
                                E.context.drawImage(n.image, 0, 0, n.original.width * l, n.original.height * l);
                                try {
                                    return u = E.elem.toDataURL(), E.length += u.length, E.cache[e] = u, u
                                } catch (e) {
                                    return n.image.src
                                }
                            }(c));
                            var p = {},
                                g = {},
                                m = function(t, i, a) {
                                    var o = 0;
                                    if (/\%/.test(t)) {
                                        var r = parseInt(t, 10) / 100,
                                            s = n.image[i] || e(n.image)[i]();
                                        o = f.ceil(-1 * s * r + a * r)
                                    } else o = z.parseValue(t);
                                    return o
                                },
                                v = {
                                    top: {
                                        top: 0
                                    },
                                    left: {
                                        left: 0
                                    },
                                    right: {
                                        left: "100%"
                                    },
                                    bottom: {
                                        top: "100%"
                                    }
                                };
                            e.each(t.position.toLowerCase().split(" "), function(e, t) {
                                "center" === t && (t = "50%"), p[e ? "top" : "left"] = t
                            }), e.each(p, function(t, i) {
                                v.hasOwnProperty(i) && e.extend(g, v[i])
                            }), p = p.top ? e.extend(p, g) : g, p = e.extend({
                                top: "50%",
                                left: "50%"
                            }, p), e(n.image).css({
                                position: "absolute",
                                top: m(p.top, "height", d),
                                left: m(p.left, "width", h)
                            }), n.show(), n.ready = !0, t.complete.call(n, n)
                        },
                        error: function() {
                            i.raise("Could not scale image: " + n.image.src)
                        },
                        timeout: 1e3
                    }), this
                }
            }, e.extend(e.easing, {
                galleria: function(e, t, i, a, n) {
                    return (t /= n / 2) < 1 ? a / 2 * t * t * t + i : a / 2 * ((t -= 2) * t * t + 2) + i
                },
                galleriaIn: function(e, t, i, a, n) {
                    return a * (t /= n) * t + i
                },
                galleriaOut: function(e, t, i, a, n) {
                    return -a * (t /= n) * (t - 2) + i
                }
            }), i.Finger = function() {
                var a = (f.abs, i.HAS3D = function() {
                        var t, i, a = n.createElement("p"),
                            o = ["webkit", "O", "ms", "Moz", ""],
                            r = 0,
                            s = "transform";
                        for (y().html.insertBefore(a, null); o[r]; r++) i = o[r] ? o[r] + "Transform" : s, void 0 !== a.style[i] && (a.style[i] = "translate3d(1px,1px,1px)", t = e(a).css(o[r] ? "-" + o[r].toLowerCase() + "-" + s : s));
                        return y().html.removeChild(a), void 0 !== t && t.length > 0 && "none" !== t
                    }()),
                    r = function() {
                        var e = "RequestAnimationFrame";
                        return t.requestAnimationFrame || t["webkit" + e] || t["moz" + e] || t["o" + e] || t["ms" + e] || function(e) {
                            t.setTimeout(e, 1e3 / 60)
                        }
                    }(),
                    s = function(i, n) {
                        if (this.config = {
                                start: 0,
                                duration: 500,
                                onchange: function() {},
                                oncomplete: function() {},
                                easing: function(e, t, i, a, n) {
                                    return -a * ((t = t / n - 1) * t * t * t - 1) + i
                                }
                            }, this.easeout = function(e, t, i, a, n) {
                                return a * ((t = t / n - 1) * t * t * t * t + 1) + i
                            }, i.children.length) {
                            var o = this;
                            e.extend(this.config, n), this.elem = i, this.child = i.children[0], this.to = this.pos = 0, this.touching = !1, this.start = {}, this.index = this.config.start, this.anim = 0, this.easing = this.config.easing, a || (this.child.style.position = "absolute", this.elem.style.position = "relative"), e.each(["ontouchstart", "ontouchmove", "ontouchend", "setup"], function(e, t) {
                                    o[t] = function(e) {
                                        return function() {
                                            e.apply(o, arguments)
                                        }
                                    }(o[t])
                                }), this.setX = function() {
                                    var e = o.child.style;
                                    if (!a) return void(e.left = o.pos + "px");
                                    e.MozTransform = e.webkitTransform = e.transform = "translate3d(" + o.pos + "px,0,0)"
                                }, e(i).on("touchstart", this.ontouchstart), e(t).on("resize", this.setup), e(t).on("orientationchange", this.setup), this.setup(),
                                function e() {
                                    r(e), o.loop.call(o)
                                }()
                        }
                    };
                return s.prototype = {
                    constructor: s,
                    setup: function() {
                        this.width = e(this.elem).width(), this.length = f.ceil(e(this.child).width() / this.width), 0 !== this.index && (this.index = f.max(0, f.min(this.index, this.length - 1)), this.pos = this.to = -this.width * this.index)
                    },
                    setPosition: function(e) {
                        this.pos = e, this.to = e
                    },
                    ontouchstart: function(e) {
                        var t = e.originalEvent.touches;
                        this.start = {
                            pageX: t[0].pageX,
                            pageY: t[0].pageY,
                            time: +new Date
                        }, this.isScrolling = null, this.touching = !0, this.deltaX = 0, o.on("touchmove", this.ontouchmove), o.on("touchend", this.ontouchend)
                    },
                    ontouchmove: function(e) {
                        var t = e.originalEvent.touches;
                        t && t.length > 1 || e.scale && 1 !== e.scale || (this.deltaX = t[0].pageX - this.start.pageX, null === this.isScrolling && (this.isScrolling = !!(this.isScrolling || f.abs(this.deltaX) < f.abs(t[0].pageY - this.start.pageY))), this.isScrolling || (e.preventDefault(), this.deltaX /= !this.index && this.deltaX > 0 || this.index == this.length - 1 && this.deltaX < 0 ? f.abs(this.deltaX) / this.width + 1.8 : 1, this.to = this.deltaX - this.index * this.width), e.stopPropagation())
                    },
                    ontouchend: function(e) {
                        this.touching = !1;
                        var t = +new Date - this.start.time < 250 && f.abs(this.deltaX) > 40 || f.abs(this.deltaX) > this.width / 2,
                            i = !this.index && this.deltaX > 0 || this.index == this.length - 1 && this.deltaX < 0;
                        this.isScrolling || this.show(this.index + (t && !i ? this.deltaX < 0 ? 1 : -1 : 0)), o.off("touchmove", this.ontouchmove), o.off("touchend", this.ontouchend)
                    },
                    show: function(e) {
                        e != this.index ? this.config.onchange.call(this, e) : this.to = -e * this.width
                    },
                    moveTo: function(e) {
                        e != this.index && (this.pos = this.to = -e * this.width, this.index = e)
                    },
                    loop: function() {
                        var e = this.to - this.pos,
                            t = 1;
                        if (this.width && e && (t = f.max(.5, f.min(1.5, f.abs(e / this.width)))), this.touching || f.abs(e) <= 1) this.pos = this.to, e = 0, this.anim && !this.touching && this.config.oncomplete(this.index), this.anim = 0, this.easing = this.config.easing;
                        else {
                            this.anim || (this.anim = {
                                start: this.pos,
                                time: +new Date,
                                distance: e,
                                factor: t,
                                destination: this.to
                            });
                            var i = +new Date - this.anim.time,
                                a = this.config.duration * this.anim.factor;
                            if (i > a || this.anim.destination != this.to) return this.anim = 0, void(this.easing = this.easeout);
                            this.pos = this.easing(null, i, this.anim.start, this.anim.distance, a)
                        }
                        this.setX()
                    }
                }, s
            }(), e.fn.galleria = function(t) {
                var a = this.selector;
                return e(this).length ? this.each(function() {
                    e.data(this, "galleria") && (e.data(this, "galleria").destroy(), e(this).find("*").hide()), e.data(this, "galleria", (new i).init(this, t))
                }) : (e(function() {
                    e(a).length ? e(a).galleria(t) : i.utils.wait({
                        until: function() {
                            return e(a).length
                        },
                        success: function() {
                            e(a).galleria(t)
                        },
                        error: function() {
                            i.raise('Init failed: Galleria could not find the element "' + a + '".')
                        },
                        timeout: 5e3
                    })
                }), this)
            }, "object" == typeof module && module && "object" == typeof module.exports ? module.exports = i : (t.Galleria = i, "function" == typeof define && define.amd && define("galleria", ["jquery"], function() {
                return i
            }))
    }(jQuery, this), (jQuery);
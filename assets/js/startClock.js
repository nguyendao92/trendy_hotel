﻿function startClock() {
    var curTime = new Date();
    var nhours = curTime.getHours();
    var nmins = curTime.getMinutes();
    var nsecn = curTime.getSeconds();
    var nday = curTime.getDay();
    var nmonth = curTime.getMonth();
    var ntoday = curTime.getDate();
    var nyear = curTime.getYear();
    var AMorPM = " ";

    if (nhours >= 12)
        AMorPM = "PM";
    else
        AMorPM = "AM";

    if (nhours >= 13)
        nhours -= 12;

    if (nhours == 0)
        nhours = 12;

    if (nsecn < 10)
        nsecn = "0" + nsecn;

    if (nmins < 10)
        nmins = "0" + nmins;

    if (nday == 0)
        nday = "Sunday";
    if (nday == 1)
        nday = "Monday";
    if (nday == 2)
        nday = "Tuesday";
    if (nday == 3)
        nday = "Wednesday";
    if (nday == 4)
        nday = "Thursday";
    if (nday == 5)
        nday = "Friday";
    if (nday == 6)
        nday = "Saturday";
    //nmonth+=1;
    if (nmonth == 0)
        nmonth = "January";
    else if (nmonth == 1)
        nmonth = "February";
    else if (nmonth == 2)
        nmonth = "March";
    else if (nmonth == 3)
        nmonth = "April";
    else if (nmonth == 4)
        nmonth = "May";
    else if (nmonth == 5)
        nmonth = "June";
    else if (nmonth == 6)
        nmonth = "July";
    else if (nmonth == 7)
        nmonth = "August";
    else if (nmonth == 8)
        nmonth = "September";
    else if (nmonth == 9)
        nmonth = "October";
    else if (nmonth == 10)
        nmonth = "November";
    else if (nmonth == 11)
        nmonth = "December";
    if (nyear <= 99)
        nyear = "19" + nyear;

    if ((nyear > 99) && (nyear < 2000))
        nyear += 1900;
    var d;
    d = document.getElementById("localTime");
    //d.innerHTML = nhours + ": " + nmins + ": " + nsecn + " " + AMorPM + "" + " - " + nday + ", " + nmonth + "-" + ntoday + "-" + nyear;
    d.innerHTML = nhours + ": " + nmins + ": " + nsecn + " " + AMorPM;
    setTimeout('startClock()', 1000);

}